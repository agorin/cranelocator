Feature: CL supports custom accounting system
    We test the CLUser models for different usage scenarios

    Background:
        Given I am using the code

    Scenario: Create new user
        Given I create a user with email "demo@mail.com" and password "demo"
        Then User "demo@mail.com" should exists
