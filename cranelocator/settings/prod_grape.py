# Settings for production environment
from .base import *
import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = (
    'crane-locator.com',
)

RAVEN_CONFIG = {
    'dsn': os.environ['SENTRY_KEY'],
}

INSTALLED_APPS = INSTALLED_APPS + (
    'raven.contrib.django.raven_compat',
)

MEDIA_ROOT = '/home/app/webapp/public/media/'
MEDIA_URL = '/media/'
STATIC_ROOT = '/home/app/webapp/public/static/'
STATIC_URL = '/static/'

CACHE_MIDDLEWARE_KEY_PREFIX = 'cranelocator'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': '127.0.0.1:11211',
        'TIMEOUT': 2592000,
        'KEY_PREFIX': CACHE_MIDDLEWARE_KEY_PREFIX,
    }
}

PROJECT_DOMAIN = 'http://crane-locator.com'
PROJECT_NAME = 'Crane Locator'

EMAIL_HOST = os.environ['EMAIL_HOST']
EMAIL_HOST_USER = os.environ['EMAIL_HOST_USER']
EMAIL_HOST_PASSWORD = os.environ['EMAIL_HOST_PASSWORD']
EMAIL_PORT = 25
DEFAULT_FROM_EMAIL = 'notification@crane-locator.com'
