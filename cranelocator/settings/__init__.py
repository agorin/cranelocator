"""
Use environment variable DJANGO_SETTINGS_MODULE for select the
right settings file.

For example:

export DJANGO_SETTINGS_MODULE=cranelocator.settings.development

"""
