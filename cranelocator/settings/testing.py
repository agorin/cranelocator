# Settings for testing environment
from .base import *

INSTALLED_APPS = INSTALLED_APPS + (
    'django_behave',
)

# TEST_RUNNER = 'django_behave.runner.DjangoBehaveTestSuiteRunner'
