# Settings for production environment
from .base import *
import os

DEBUG = False
TEMPLATE_DEBUG = DEBUG

ALLOWED_HOSTS = (
    'cranelocator.herokuapp.com',
)

RAVEN_CONFIG = {
    'dsn': os.environ['SENTRY_KEY'],
}

INSTALLED_APPS = (
    'collectfast',
) + INSTALLED_APPS + (
    'storages',
    'raven.contrib.django.raven_compat',
)

GEOS_LIBRARY_PATH = os.environ.get('GEOS_LIBRARY_PATH')
GDAL_LIBRARY_PATH = os.environ.get('GDAL_LIBRARY_PATH')


# Amazon S3 settings.
AWS_ACCESS_KEY_ID = os.environ.get("AWS_ACCESS_KEY_ID", "")
AWS_SECRET_ACCESS_KEY = os.environ.get("AWS_SECRET_ACCESS_KEY", "")
AWS_STORAGE_BUCKET_NAME = os.environ.get("AWS_STORAGE_BUCKET_NAME", "")
AWS_S3_HOST = os.environ.get("AWS_S3_HOST", "")
AWS_HEADERS = {
    "Cache-Control": "public, max-age=86400",
}

AWS_AUTO_CREATE_BUCKET = True
AWS_S3_FILE_OVERWRITE = False
AWS_QUERYSTRING_AUTH = False
AWS_S3_SECURE_URLS = True
AWS_REDUCED_REDUNDANCY = False
AWS_IS_GZIPPED = False

AWS_S3_CUSTOM_DOMAIN = '%s.s3.amazonaws.com' % AWS_STORAGE_BUCKET_NAME

AWS_PRELOAD_METADATA = True
STATICFILES_LOCATION = 'static'
STATICFILES_STORAGE = 'apps.core.storage.StaticStorage'
STATIC_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, STATICFILES_LOCATION)

MEDIAFILES_LOCATION = 'media'
MEDIA_URL = "https://%s/%s/" % (AWS_S3_CUSTOM_DOMAIN, MEDIAFILES_LOCATION)
DEFAULT_FILE_STORAGE = 'apps.core.storage.MediaStorage'


CACHE_MIDDLEWARE_KEY_PREFIX = 'cranelocator'
CACHES = {
    'default': {
        'BACKEND': 'django.core.cache.backends.memcached.MemcachedCache',
        'LOCATION': os.environ.get('MEMCACHIER_SERVERS', ''),
        'OPTIONS': {
            'username': os.environ.get('MEMCACHIER_USERNAME', ''),
            'password': os.environ.get('MEMCACHIER_PASSWORD', ''),
        },
        'TIMEOUT': 2592000,
        'KEY_PREFIX': CACHE_MIDDLEWARE_KEY_PREFIX,
    }
}

PROJECT_DOMAIN = 'http://crane-locator.com'
PROJECT_NAME = 'Crane Locator'

# Email settings.
EMAIL_HOST = "smtp.sendgrid.net"
EMAIL_HOST_USER = os.environ.get("SENDGRID_USERNAME", "")
EMAIL_HOST_PASSWORD = os.environ.get("SENDGRID_PASSWORD", "")
EMAIL_PORT = 25
EMAIL_USE_TLS = False
DEFAULT_FROM_EMAIL = 'notification@crane-locator.com'

SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
