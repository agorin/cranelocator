from django.conf.urls import patterns, include, url
from django.contrib import admin
from django.views.generic import TemplateView
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns


urlpatterns = patterns(
    '',
    url(r'^$', 'cranelocator.views.home', name='home'),

    url(r'^', include('apps.accounts.urls')),
    url(r'^', include('apps.invitation.urls')),
    url(r'^', include('apps.companies.urls')),
    url(r'^', include('apps.equipment.urls')),
    url(r'^admin/', include(admin.site.urls)),
    (r'^ckeditor/', include('ckeditor.urls')),

    url('^robots\.txt$', TemplateView.as_view(
        template_name='robots.txt', content_type='text/plain')),
    url('^humans\.txt$', TemplateView.as_view(
        template_name='humans.txt', content_type='text/plain')),
)


# This is only needed when using runserver.
if settings.DEBUG:
    urlpatterns = patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve',  # NOQA
            {'document_root': settings.MEDIA_ROOT, 'show_indexes': True}),
        ) + staticfiles_urlpatterns() + urlpatterns  # NOQA

    import debug_toolbar
    urlpatterns += patterns(
        '',
        url(r'^__debug__/', include(debug_toolbar.urls)),
    )
