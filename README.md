[![Build Status](https://drone.io/bitbucket.org/the7bits/cranelocator/status.png)](https://drone.io/bitbucket.org/the7bits/cranelocator/latest)

## Required libraries

    sudo apt-get install libgeos-c1 libmemcached-dev

## How to turn on PostGIS

Use the next command after DB creation:

    create extension postgis;

## What to do after install

Add the next user groups:

 * `Verified`
 * `Can send invitations`

## How to set necessary settings

Project use environment variables for different settings. Such scenario
is perfect for production deployment to Heroku, but also suitable for
other platforms.

The next environment variables is required everywhere:

  * `DJANGO_SETTINGS_MODULE` - should point to the right settings module.
  * `SECRET_KEY` - secret key. You can use command `openssl rand -base64 32` for
    generate it.
  * `DATABASE_URL` - location of your database. For example: `postgis://postgres:123456@localhost:5432/cranelocator`

Variables required for Grape:

  * `EMAIL_HOST`
  * `EMAIL_HOST_USER`
  * `EMAIL_HOST_PASSWORD`
  * `EMAIL_PORT`
  * `SENTRY_KEY`

Variables required for Heroku:

  * `AWS_ACCESS_KEY_ID`
  * `AWS_SECRET_ACCESS_KEY`
  * `AWS_STORAGE_BUCKET_NAME`
  * `AWS_S3_HOST`
  * `S3_USE_SIGV4=True` - if use S3 in Frankfurt region


## How to setup environment variables on development machine

The right place for setting the environment variables for local development
is a script for activation your Python virtual environment.
For example: `env_py34/bin/activate`. Add the next lines to the end of that file:

    export DJANGO_SETTINGS_MODULE=cranelocator.settings.development
    export SECRET_KEY=1c3-cr3-am-15-yummy
    export DATABASE_URL=postgis://postgres:123456@localhost:5432/cranelocator

## How to setup environment variables on Grape

Login to container as root user and go to the `/etc/profile.d` directory.
Create file `webapp.sh` in that directory and declare environment
variables there with `export` command.

## How to setup environment variables on Heroku

Use your web console. You also can use command line, for example:

    heroku config:set SECRET_KEY=`openssl rand -base64 32`

## Notes for Heroku

You should set custom builpack:

    heroku config:set BUILDPACK_URL=https://github.com/ddollar/heroku-buildpack-multi.git
    heroku stack:set cedar

## Notes for DroneIO

If you use DroneIO as a continious integration service use the next
environment variables:

    DJANGO_SETTINGS_MODULE=cranelocator.settings.testing
    SECRET_KEY=yummy
    DATABASE_URL=postgis://postgres@127.0.0.1:5432/cranelocator

And the next build script:

    sudo /etc/init.d/postgresql stop

    sudo apt-get --force-yes -fuy remove --purge postgresql postgresql-9.1 postgresql-client
    echo "deb http://apt.postgresql.org/pub/repos/apt/ precise-pgdg main" | sudo tee /etc/apt/sources.list.d/postgis.list
    wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install postgresql-9.3 postgresql-9.3-postgis-2.1 postgresql-client-9.3

    sudo /bin/bash -c "cat <<EOF > /etc/postgresql/9.3/main/pg_hba.conf
    local   all             postgres                                trust
    local   all             all                                     trust
    host    all             all             127.0.0.1/32            trust
    host    all             all             ::1/128                 trust
    EOF"

    sudo /etc/init.d/postgresql restart

    psql -c 'create database cranelocator;' -U postgres
    psql cranelocator -c 'create extension postgis;' -U postgres

    pip install -r requirements.txt --use-mirrors
    python3 manage.py migrate
    python3 manage.py test

## How to install AWS command line client

Don't use `s3cmd` package from the official Ubuntu repository, there is old version.
Install from PyPi:

    pip install --use-mirrors s3cmd

Then add config file `.s3cfg` to the home directory (for example `/root`) with
the next content:

    [default]
    access_key=<ACCESS_KEY>
    secret_key=<SECRET_KEY>
    bucket_location=eu-central-1

## How to upload media files from dedicated server to Amazon S3

Example (run it from the directory with `media` folder):

    s3cmd --recursive sync media/* s3://cranelocator/media/

