from django.db.models.signals import post_migrate
from django.contrib.auth import get_user_model
from ..models import InvitationStats


def create_stats_for_existing_users(sender, **kwargs):
    """
    Create `InvitationStats` objects for all users after a `sycndb`

    """
    count = 0
    user_model = get_user_model()
    for user in user_model.objects.filter(invitation_stats__isnull=True):
        InvitationStats.objects.create(user=user)
        count += 1
    if count > 0:
        print ("Created InvitationStats for %s existing Users" % count)


post_migrate.connect(create_stats_for_existing_users, sender='invitation')
