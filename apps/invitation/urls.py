from django.conf.urls import patterns, url
from django.views.generic import TemplateView, RedirectView
from django.contrib.auth.decorators import login_required
from .settings import INVITE_ONLY


urlpatterns = patterns(
    '',
    url(r'^invitation/$',
        login_required(TemplateView.as_view(
            template_name='invitation/invitation_home.html')),
        name='invitation_home'),
    url(r'^invitation/invite/$',
        'apps.invitation.views.invite',
        name='invitation_invite'),
    url(r'^invitation/invite/complete/$',
        login_required(TemplateView.as_view(
            template_name='invitation/invitation_complete.html')),
        name='invitation_complete'),
    url(r'^invitation/invite/unavailable/$',
        login_required(TemplateView.as_view(
            template_name='invitation/invitation_unavailable.html')),
        name='invitation_unavailable'),
    url(r'^invitation/accept/complete/$',
        TemplateView.as_view(
            template_name='invitation/invitation_registered.html'),
        name='invitation_registered'),
    url(r'^invitation/accept/(?P<invitation_key>\w+)/$',
        'apps.invitation.views.register',
        name='invitation_register'),
)


if INVITE_ONLY:
    urlpatterns += patterns(
        '',
        url(r'^register/$',
            RedirectView.as_view(
                url='../invitation/invite_only/',
                permanent=False),
            name='registration_register'),
        url(r'^invitation/invite_only/$',
            TemplateView.as_view(
                template_name='invitation/invite_only.html'),
            name='invitation_invite_only'),
        url(r'^invitation/reward/$',
            'apps.invitation.views.reward',
            name='invitation_reward'),
    )
