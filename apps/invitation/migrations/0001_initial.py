# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Invitation',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('email', models.EmailField(verbose_name='Email', max_length=75)),
                ('key', models.CharField(unique=True, verbose_name='Invitation key', max_length=40)),
                ('date_invited', models.DateTimeField(default=datetime.datetime.now, verbose_name='Date invited')),
                ('user', models.ForeignKey(related_name='invitations', to=settings.AUTH_USER_MODEL)),
            ],
            options={
                'verbose_name': 'Invitation',
                'verbose_name_plural': 'Invitations',
                'ordering': ('-date_invited',),
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='InvitationStats',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('available', models.IntegerField(default=10, verbose_name='Available invitations')),
                ('sent', models.IntegerField(default=0, verbose_name='Invitations sent')),
                ('accepted', models.IntegerField(default=0, verbose_name='Invitations accepted')),
                ('user', models.OneToOneField(to=settings.AUTH_USER_MODEL, related_name='invitation_stats')),
            ],
            options={
                'verbose_name': 'Invitation stats',
                'verbose_name_plural': 'Invitation stats',
                'ordering': ('-user',),
            },
            bases=(models.Model,),
        ),
    ]
