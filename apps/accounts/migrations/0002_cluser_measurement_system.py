# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cluser',
            name='measurement_system',
            field=models.CharField(verbose_name='Measurement system', default='metric', max_length=20, choices=[('metric', 'Metric'), ('imperial', 'Imperial')]),
            preserve_default=True,
        ),
    ]
