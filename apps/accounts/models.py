from django.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.models import (
    AbstractBaseUser,
    BaseUserManager,
    PermissionsMixin,
)


class CLUserManager(BaseUserManager):
    """
    Creates and saves a User with the given email, full_name and password.
    """
    def _create_user(self, email, full_name, password,
                     is_staff, is_superuser, **extra_fields):
        """
        Creates and saves a User with the given username, email and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError('The given username must be set')
        email = self.normalize_email(email)
        user = self.model(email=email,
                          is_staff=is_staff, is_active=True,
                          full_name=full_name,
                          is_superuser=is_superuser,
                          date_joined=now,
                          last_login=now,
                          **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email, full_name, password=None, **extra_fields):
        return self._create_user(
            email, full_name, password,
            False, False, **extra_fields)

    def create_superuser(
            self, email, full_name, password=None, **extra_fields):
        """
        Creates and saves a superuser with the given email,
        full_name and password.
        """
        return self._create_user(
            email, full_name, password,
            True, True, **extra_fields)


class CLUser(AbstractBaseUser, PermissionsMixin):
    """
    A model which implements the authentication model.

    Email, password and full_name are required. Other fields are optional.

    Email field is used for logging in.
    """
    MEASUREMENT_SYSTEM_CHOICES = (
        ('metric', _('Metric')),
        ('imperial', _('Imperial')),
    )

    email = models.EmailField(
        _('Email'), max_length=255, unique=True)
    full_name = models.CharField(_('Full name'), max_length=100)
    measurement_system = models.CharField(
        _('Measurement system'), max_length=20, default='metric',
        choices=MEASUREMENT_SYSTEM_CHOICES,
    )

    is_staff = models.BooleanField(
        _('Staff status'), default=False,
        help_text=_(
            'Designates whether the user can log into this admin site.'))
    is_active = models.BooleanField(
        _('Active'), default=True,
        help_text=_('Designates whether this user should be treated as '
                    'active. Unselect this instead of deleting accounts.'))

    date_joined = models.DateTimeField(_('Date joined'), default=timezone.now)

    objects = CLUserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['full_name']

    class Meta:
        verbose_name = _(u'User')
        verbose_name_plural = _(u'Users')

    def __str__(self):
        return self.full_name or self.email

    def get_full_name(self):
        return self.full_name

    def get_short_name(self):
        return self.full_name

    def can_send_invitations(self):
        return self.groups.filter(name='Can send invitations').exists()

    def is_verified(self):
        return self.groups.filter(name='Verified').exists()

    def get_company_roles(self):
        return self.company_roles.all()
