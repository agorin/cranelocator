from django.conf.urls import patterns, url
from django.contrib.auth.views import (
    login,
    logout,
    password_change,
    password_change_done,
    password_reset,
    password_reset_done,
    password_reset_confirm,
    password_reset_complete,
)
from django.views.generic import TemplateView
from .decorators import ajax_required
from .views import (
    profile_view,
    MyProfileUpdateView,
)


urlpatterns = patterns(
    '',
    url(
        r'^login/$', login,
        {'template_name': 'accounts/login.html'}, name='login'),
    url(r'^logout/$', logout, {'next_page': '/'}, name='logout'),

    url(
        r'^password-change/$',
        password_change,
        {'template_name': 'accounts/password_change_form.html'},
        name='password_change'),
    url(
        r'^password-change/done/$',
        password_change_done,
        {'template_name': 'accounts/password_change_done.html'},
        name='password_change_done'),
    url(
        r'^password-reset/$',
        password_reset,
        {'template_name': 'accounts/password_reset_form.html'},
        name='password_reset'),
    url(
        r'^password-reset/done/$',
        password_reset_done,
        {'template_name': 'accounts/password_reset_done.html'},
        name='password_reset_done'),
    url(
        r'^password-reset/(?P<uidb64>.+)/(?P<token>.+)/confirm/$',
        password_reset_confirm,
        {'template_name': 'accounts/password_reset_confirm.html'},
        name='password_reset_confirm'),
    url(
        r'^password-reset/complete/$',
        password_reset_complete,
        {'template_name': 'accounts/password_reset_complete.html'},
        name='password_reset_complete'),

    url(r'^user-toolbar/$', ajax_required(
        TemplateView.as_view(template_name='accounts/user_toolbar.html'))),
    url(
        r'^edit-profile/$',
        MyProfileUpdateView.as_view(),
        name='edit_my_profile'),
    url(
        r'^profile/$', profile_view,
        name='my_profile'),
)
