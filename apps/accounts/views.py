from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.shortcuts import render_to_response, redirect, render
from django.contrib.auth.decorators import login_required
from django.views.generic.detail import DetailView
from django.views.generic import UpdateView
from django.conf import settings
from django.contrib.messages.views import SuccessMessageMixin
from .forms import EditCLUserForm
from .models import CLUser


@login_required
def profile_view(request):
    return render(
        request,
        'accounts/my_profile.html',
        {
            'user': request.user,
        })


class MyProfileUpdateView(SuccessMessageMixin, UpdateView):
    model = CLUser
    template_name = 'accounts/edit_my_profile.html'
    form_class = EditCLUserForm
    success_message = _(u"Your profile settings was changed.")

    def get_object(self):
        return CLUser.objects.get(pk=self.request.user.pk)

    def get_success_url(self):
        return reverse(
            'cluser_details', args=(self.request.user.pk,))
