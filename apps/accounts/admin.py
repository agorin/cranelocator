from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.admin import UserAdmin

from .models import CLUser
from .forms import CLUserChangeForm, CLUserCreationForm


class CLUserAdmin(UserAdmin):
    add_form_template = 'accounts/admin/auth/user/add_form.html'
    fieldsets = (
        (None, {
            'fields': (
                'email', 'full_name',
                'password',
            )}),
        (_(u'Permissions'), {
            'fields': (
                'is_active', 'is_staff',
                'is_superuser',
                'groups',
                'user_permissions',
            )}),
        (_(u'Important dates'), {
            'fields': ('last_login', 'date_joined')}),
    )
    # exclude = ('last_name', 'first_name', 'username')
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'full_name', 'password1', 'password2')
        }),
    )

    list_display = (
        'email', 'full_name', 'is_staff', 'is_superuser', 'is_active')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('full_name', 'email')
    ordering = ('email',)
    date_hierarchy = 'date_joined'
    filter_horizontal = ('groups', 'user_permissions',)

    form = CLUserChangeForm
    add_form = CLUserCreationForm

admin.site.register(CLUser, CLUserAdmin)
