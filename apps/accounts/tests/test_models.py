from django.test import TestCase
from ..models import CLUser


class CLUserTests(TestCase):
    def setUp(self):
        self.u1 = CLUser.objects.create_user(
            'demo@mail.com', 'John Doe', 'demo')

    def test_create_user(self):
        u = CLUser.objects.get(email='demo@mail.com')
        self.assertEquals(u.full_name, 'John Doe')
