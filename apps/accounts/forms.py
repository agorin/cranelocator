from django import forms
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.forms import (
    UserCreationForm,
    UserChangeForm,
    ReadOnlyPasswordHashField,
)
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from .models import CLUser


class CLUserChangeForm(UserChangeForm):
    password = ReadOnlyPasswordHashField(
        label=_('Password'),
        help_text=_(
            u'Raw passwords are not stored, so there is no way to see '
            u'this user\'s password, but you can change the password '
            u'using <a href="password/">this form</a>.'))

    class Meta:
        model = CLUser
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # del self.fields['username']
        # f = self.fields.get('user_permissions', None)
        # if f is not None:
        #     f.queryset = f.queryset.select_related('content_type')

    def clean_password(self):
        return self.initial['password']


class CLUserCreationForm(UserCreationForm):
    """
    A form that creates a user, with no privileges,
    from the given email, full_name and password.
    """
    error_messages = {
        'duplicate_email': _("A user with that email already exists."),
        'password_mismatch': _("The two password fields didn't match."),
    }
    password1 = forms.CharField(
        label=_("Password"),
        widget=forms.PasswordInput)
    password2 = forms.CharField(
        label=_("Password confirmation"),
        widget=forms.PasswordInput,
        help_text=_("Enter the same password as above, for verification."))

    class Meta:
        model = CLUser
        fields = '__all__'

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        del self.fields['username']

    def clean_email(self):
        email = self.cleaned_data['email']
        try:
            CLUser.objects.get(email=email)
        except CLUser.DoesNotExist:
            return email
        raise forms.ValidationError(self.error_messages['duplicate_email'])

    def clean_password2(self):
        password1 = self.cleaned_data.get("password1")
        password2 = self.cleaned_data.get("password2")
        if password1 and password2 and password1 != password2:
            raise forms.ValidationError(
                self.error_messages['password_mismatch'])
        return password2

    def save(self, commit=True):
        user = super().save(commit=False)
        user.set_password(self.cleaned_data["password1"])
        if commit:
            user.save()
        return user


class EditCLUserForm(forms.ModelForm):
    class Meta:
        model = CLUser
        fields = ('email', 'full_name', 'measurement_system')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'email',
            'full_name',
            'measurement_system',
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class CLUserRegistrationForm(CLUserCreationForm):
    class Meta:
        model = CLUser
        fields = ('email', 'full_name')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'email',
            'full_name',
            'password1',
            'password2',
            ButtonHolder(
                Submit('submit', _('Create user'), css_class='btn btn-primary')
            )
        )
