# from django.http import HttpResponseForbidden
from django.shortcuts import get_object_or_404
from django.utils.translation import ugettext as _
from django.core.urlresolvers import reverse
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.contrib.auth.decorators import login_required
from django.utils.text import slugify
from django.views.generic.detail import DetailView
from django.views.generic import UpdateView, ListView
from django.contrib.messages.views import SuccessMessageMixin

from .models import (
    Company,
    CompanyUserRole,
)
from .forms import (
    CreateCompanyForm,
    ChangeBusinessSegmentsForm,
    ChangeScopeOfOperationForm,
    ChangeCompanyLogoForm,
    ChangeCompanyDescriptionForm,
    ChangeCompanyAddressesForm,
    ChangeCompanyContactsForm,
)
from .decorators import (
    company_staff_required,
    company_owner_required,
)


class CompanyListView(ListView):
    model = Company
    context_object_name = 'companies'
    paginate_by = 20

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)


class CompanyDetailView(DetailView):
    model = Company
    context_object_name = 'company'

    @method_decorator(login_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        can_manage = False

        if self.request.user.is_authenticated():
            _company = self.get_object()
            try:
                role = _company.roles.get(user=self.request.user)
                can_manage = role.role in ['owner', 'manager']
            except CompanyUserRole.DoesNotExist:
                can_manage = False

        context['can_manage'] = can_manage
        return context


def company_sidebar(request, slug):
    user = request.user
    company = get_object_or_404(Company, slug__exact=slug)
    can_manage = False

    if user.is_authenticated():
        try:
            role = company.roles.get(user=user)
            can_manage = role.role in ['owner', 'manager']
        except CompanyUserRole.DoesNotExist:
            can_manage = False

    return render(
        request,
        'companies/_company_sidebar.html',
        {
            'user': user,
            'company': company,
            'can_manage': can_manage,
        })


@login_required
def create_company(request):
    if request.method == 'POST':
        form = CreateCompanyForm(request.POST)
        if form.is_valid():
            _company = form.save(commit=False)
            _company.slug = '-'.join([
                slugify(_company.name),
                slugify(str(_company.invoice_country)),
                slugify(_company.invoice_city),
            ])
            _company.visit_country = _company.invoice_country
            _company.save()
            form.save_m2m()
            CompanyUserRole.objects.create(
                company=_company,
                user=request.user,
                role='owner',
            )
            return redirect(_company.get_absolute_url())
    else:
        form = CreateCompanyForm()

    return render(
        request,
        'companies/company_add.html',
        {
            'user': request.user,
            'form': form,
        })


@company_staff_required
def manage_company(request, slug):
    user = request.user
    company = Company.objects.get(slug__exact=slug)
    role = CompanyUserRole.objects.get(
        company=company, user=user)

    return render(
        request,
        'companies/company_manage.html',
        {
            'user': user,
            'role': role,
            'company': company,
        })


class UpdateBusinessSegmentsView(SuccessMessageMixin, UpdateView):
    model = Company
    form_class = ChangeBusinessSegmentsForm
    template_name_suffix = '_update_business_segments'
    success_message = _('Company business segments were saved.')

    @method_decorator(company_staff_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(
            'company_manage',
            kwargs={'slug': self.kwargs['slug']})


class UpdateScopeOfOperationView(SuccessMessageMixin, UpdateView):
    model = Company
    form_class = ChangeScopeOfOperationForm
    template_name_suffix = '_update_scope_of_operation'
    success_message = _('Company scope of operation was saved.')

    @method_decorator(company_staff_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(
            'company_manage',
            kwargs={'slug': self.kwargs['slug']})


class UpdateCompanyLogoView(SuccessMessageMixin, UpdateView):
    model = Company
    form_class = ChangeCompanyLogoForm
    template_name_suffix = '_update_logo'
    success_message = _('Company logo was saved.')

    @method_decorator(company_staff_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(
            'company_manage',
            kwargs={'slug': self.kwargs['slug']})


class UpdateCompanyDescriptionView(SuccessMessageMixin, UpdateView):
    model = Company
    form_class = ChangeCompanyDescriptionForm
    template_name_suffix = '_update_description'
    success_message = _('Company description was successfully saved.')

    @method_decorator(company_staff_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(
            'company_manage_description',
            kwargs={'slug': self.kwargs['slug']})


class UpdateCompanyAddressesView(SuccessMessageMixin, UpdateView):
    model = Company
    form_class = ChangeCompanyAddressesForm
    template_name_suffix = '_update_addresses'
    success_message = _('Company addresses was successfully saved.')

    @method_decorator(company_staff_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(
            'company_manage_addresses',
            kwargs={'slug': self.kwargs['slug']})


class UpdateCompanyContactsView(SuccessMessageMixin, UpdateView):
    model = Company
    form_class = ChangeCompanyContactsForm
    template_name_suffix = '_update_contacts'
    success_message = _('Company contacts was successfully saved.')

    @method_decorator(company_staff_required)
    def dispatch(self, *args, **kwargs):
        return super().dispatch(*args, **kwargs)

    def get_success_url(self):
        return reverse(
            'company_manage_contacts',
            kwargs={'slug': self.kwargs['slug']})


@company_staff_required
def manage_company_staff(request, slug):
    user = request.user
    company = Company.objects.get(slug__exact=slug)
    role = CompanyUserRole.objects.get(
        company=company, user=user)

    return render(
        request,
        'companies/company_manage_staff.html',
        {
            'user': user,
            'role': role,
            'company': company,
        })


@company_staff_required
def manage_company_equipment(request, slug):
    from apps.equipment.models import EquipmentCategory, Equipment
    company = Company.objects.get(slug__exact=slug)

    top_categories = EquipmentCategory.objects.filter(level=0)
    equipment = Equipment.objects.filter(owner=company)
    used_categories = EquipmentCategory.objects.filter(
        equipments__in=equipment).distinct()

    for u in used_categories:
        u.equipment_amount = u.equipments.filter(owner=company).count()

    for t in top_categories:
        t.equipment_amount = Equipment.objects.filter(
            owner=company, category__in=t.get_descendants())\
            .distinct().count()

    return render(
        request,
        'companies/company_manage_equipment.html',
        {
            'company': company,
            'top_categories': top_categories,
            'used_categories': used_categories,
        })


@company_staff_required
def manage_company_equipment_category(request, slug, category_slug):
    from apps.equipment.models import EquipmentCategory, Equipment
    company = Company.objects.get(slug__exact=slug)

    category = get_object_or_404(EquipmentCategory, slug__exact=category_slug)
    equipment = Equipment.objects.filter(
        category=category, owner=company)

    children = category.children.all()
    for t in children:
        t.equipment_amount = Equipment.objects.filter(
            owner=company, category__in=t.get_descendants(include_self=True))\
            .distinct().count()

    return render(
        request,
        'companies/company_manage_equipment_category.html',
        {
            'company': company,
            'category': category,
            'equipment': equipment,
            'children': children,
        })


@company_staff_required
def manage_company_add_equipment(request, slug, category_slug):
    from apps.equipment.models import (
        EquipmentCategory,
        Equipment,
        EquipmentPropertyValue,
        ManufacturerRequest,
    )
    from apps.equipment.forms import AddEquipmentToCompanyForm
    company = Company.objects.get(slug__exact=slug)

    category = get_object_or_404(EquipmentCategory, slug__exact=category_slug)

    if request.method == 'POST':
        form = AddEquipmentToCompanyForm(
            request.POST,
            request.FILES,
            company=company,
            equipment_category=category,
        )
        if form.is_valid():
            _equipment = form.save(commit=False)
            _equipment.owner = company
            _equipment.category = category
            _equipment.save()
            form.save_m2m()

            _property_values = _equipment.property_values.all()
            for _pv in _property_values:
                _value = form.cleaned_data.get(
                    'property-%s' % _pv.equipment_property.code)
                if _value:
                    _pv.value = _value
                    _pv.save()

            if _equipment.manufacturer.slug == 'other':
                _mn = form.cleaned_data.get('other_manufacturer')
                if _mn:
                    _mr, created = ManufacturerRequest.objects.get_or_create(
                        name=_mn)
                    _mr.requested_for.add(_equipment)

            return redirect(reverse(
                'company_manage_equipment_category',
                kwargs={
                    'slug': slug,
                    'category_slug': category_slug,
                }))
    else:
        form = AddEquipmentToCompanyForm(
            company=company,
            equipment_category=category,
        )

    return render(
        request,
        'companies/company_manage_add_equipment.html',
        {
            'company': company,
            'category': category,
            'form': form,
        })
