# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0016_auto_20150327_1232'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='invoice_city',
            field=models.CharField(verbose_name='City', max_length=255),
            preserve_default=True,
        ),
    ]
