# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0007_businesssegment'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='businesssegment',
            options={'ordering': ('label',), 'verbose_name': 'Business segment', 'verbose_name_plural': 'Business segments'},
        ),
        migrations.RemoveField(
            model_name='company',
            name='category_lifting',
        ),
        migrations.RemoveField(
            model_name='company',
            name='category_rigging',
        ),
        migrations.RemoveField(
            model_name='company',
            name='category_sea',
        ),
        migrations.RemoveField(
            model_name='company',
            name='category_transport',
        ),
        migrations.AddField(
            model_name='company',
            name='business_segments',
            field=models.ManyToManyField(null=True, blank=True, verbose_name='Business segments', to='companies.BusinessSegment'),
            preserve_default=True,
        ),
    ]
