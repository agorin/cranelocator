# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0002_companyuserrole'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='companyuserrole',
            options={'verbose_name_plural': 'Company user roles', 'verbose_name': 'Company user role'},
        ),
        migrations.AlterField(
            model_name='companyuserrole',
            name='company',
            field=models.ForeignKey(verbose_name='Company', to='companies.Company', related_name='roles'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='companyuserrole',
            name='user',
            field=models.ForeignKey(verbose_name='User', to=settings.AUTH_USER_MODEL, related_name='company_roles'),
            preserve_default=True,
        ),
    ]
