# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0006_auto_20150309_1736'),
    ]

    operations = [
        migrations.CreateModel(
            name='BusinessSegment',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('label', models.CharField(verbose_name='Label', max_length=100)),
                ('code', models.SlugField(verbose_name='Code')),
            ],
            options={
                'verbose_name': 'Business segment',
                'verbose_name_plural': 'Business segments',
            },
            bases=(models.Model,),
        ),
    ]
