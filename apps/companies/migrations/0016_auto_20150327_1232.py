# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django_countries.fields


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0015_company_verified'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='address',
        ),
        migrations.RemoveField(
            model_name='company',
            name='phones',
        ),
        migrations.AddField(
            model_name='company',
            name='cp1_email',
            field=models.EmailField(default='', blank=True, verbose_name='Email', max_length=75),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp1_name',
            field=models.CharField(default='', blank=True, verbose_name='Full name', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp1_phone',
            field=models.CharField(help_text='Phone number including international code.', default='', blank=True, verbose_name='Phone', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp1_position',
            field=models.CharField(default='', blank=True, verbose_name='Position', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp1_skype',
            field=models.CharField(default='', blank=True, verbose_name='Skype', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp2_email',
            field=models.EmailField(default='', blank=True, verbose_name='Email', max_length=75),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp2_name',
            field=models.CharField(default='', blank=True, verbose_name='Full name', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp2_phone',
            field=models.CharField(help_text='Phone number including international code.', default='', blank=True, verbose_name='Phone', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp2_position',
            field=models.CharField(default='', blank=True, verbose_name='Position', max_length=100),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='cp2_skype',
            field=models.CharField(default='', blank=True, verbose_name='Skype', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='fax',
            field=models.CharField(help_text='Fax number including international code.', default='', blank=True, verbose_name='Fax', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='invoice_additional_info',
            field=models.TextField(default='', blank=True, verbose_name='Additional info'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='invoice_city',
            field=models.CharField(default='', blank=True, verbose_name='City', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='invoice_country',
            field=django_countries.fields.CountryField(default='UA', verbose_name='Country', max_length=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company',
            name='invoice_house_number',
            field=models.CharField(default='', blank=True, verbose_name='House number', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='invoice_post_code',
            field=models.CharField(default='', blank=True, verbose_name='Post code', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='invoice_street',
            field=models.CharField(default='', blank=True, verbose_name='Street', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='pnone1',
            field=models.CharField(help_text='Phone number including international code.', default='', blank=True, verbose_name='First phone', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='pnone2',
            field=models.CharField(help_text='Phone number including international code.', default='', blank=True, verbose_name='Second phone', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='visit_additional_info',
            field=models.TextField(default='', blank=True, verbose_name='Additional info'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='visit_city',
            field=models.CharField(default='', blank=True, verbose_name='City', max_length=255),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='visit_country',
            field=django_countries.fields.CountryField(default='UA', verbose_name='Country', max_length=2),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company',
            name='visit_house_number',
            field=models.CharField(default='', blank=True, verbose_name='House number', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='visit_post_code',
            field=models.CharField(default='', blank=True, verbose_name='Post code', max_length=50),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='visit_street',
            field=models.CharField(default='', blank=True, verbose_name='Street', max_length=255),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='scope_of_operation',
            field=models.ManyToManyField(to='companies.ScopeOfOperation', blank=True, verbose_name='Scope of region operation', null=True),
            preserve_default=True,
        ),
    ]
