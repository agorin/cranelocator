# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0005_auto_20150306_1425'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='category_lifting',
            field=models.BooleanField(verbose_name='Lifting', default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='category_rigging',
            field=models.BooleanField(verbose_name='Rigging', default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='category_sea',
            field=models.BooleanField(verbose_name='Sea', default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='category_transport',
            field=models.BooleanField(verbose_name='Transport', default=False),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='scope_of_operation',
            field=models.CharField(verbose_name='Scope of operation', blank=True, max_length=250, default='Global'),
            preserve_default=True,
        ),
    ]
