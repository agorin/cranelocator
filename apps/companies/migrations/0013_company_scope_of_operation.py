# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0012_remove_company_scope_of_operation'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='scope_of_operation',
            field=models.ManyToManyField(blank=True, null=True, verbose_name='Scope of operation', to='companies.ScopeOfOperation'),
            preserve_default=True,
        ),
    ]
