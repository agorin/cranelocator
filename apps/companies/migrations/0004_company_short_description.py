# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0003_auto_20150304_0917'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='short_description',
            field=ckeditor.fields.RichTextField(verbose_name='Short description', default='Short description'),
            preserve_default=False,
        ),
    ]
