# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import apps.companies.models


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0010_auto_20150325_1449'),
    ]

    operations = [
        migrations.CreateModel(
            name='ScopeOfOperation',
            fields=[
                ('id', models.AutoField(primary_key=True, auto_created=True, verbose_name='ID', serialize=False)),
                ('label', models.CharField(max_length=100, verbose_name='Label')),
                ('code', models.SlugField(verbose_name='Code')),
            ],
            options={
                'ordering': ('label',),
                'verbose_name': 'Scope of operation',
                'verbose_name_plural': 'Scopes of operation',
            },
            bases=(models.Model,),
        ),
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Name', validators=[apps.companies.models.validate_company_name]),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='vat',
            field=models.CharField(max_length=100, verbose_name='VAT Number', validators=[apps.companies.models.validate_vat]),
            preserve_default=True,
        ),
    ]
