# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0014_auto_20150326_1639'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='verified',
            field=models.BooleanField(verbose_name='Verified', default=False),
            preserve_default=True,
        ),
    ]
