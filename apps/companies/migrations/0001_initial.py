# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name')),
                ('logo', sorl.thumbnail.fields.ImageField(upload_to='companies/logos', verbose_name='Logo')),
                ('description', ckeditor.fields.RichTextField(verbose_name='Description')),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('last_change', models.DateTimeField(auto_now=True, verbose_name='Last change')),
            ],
            options={
                'verbose_name_plural': 'Companies',
                'verbose_name': 'Company',
            },
            bases=(models.Model,),
        ),
    ]
