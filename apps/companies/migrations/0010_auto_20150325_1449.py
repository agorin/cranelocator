# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0009_company_vat'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='description',
            field=ckeditor.fields.RichTextField(verbose_name='Description', default='', blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='logo',
            field=sorl.thumbnail.fields.ImageField(verbose_name='Logo', null=True, blank=True, upload_to='companies/logos'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='short_description',
            field=ckeditor.fields.RichTextField(verbose_name='Short description', default='', blank=True),
            preserve_default=True,
        ),
    ]
