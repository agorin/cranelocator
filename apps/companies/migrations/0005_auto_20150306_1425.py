# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0004_company_short_description'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='address',
            field=ckeditor.fields.RichTextField(default='', verbose_name='Address', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='depots',
            field=models.IntegerField(verbose_name='Depots', default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='email',
            field=models.EmailField(max_length=75, default='', verbose_name='Email', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='employees',
            field=models.IntegerField(verbose_name='Employees', default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='meta_description',
            field=models.TextField(default='{{company.name}}', verbose_name='META description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='meta_title',
            field=models.CharField(max_length=250, default='{{site_name}} - {{company.name}}', verbose_name='META title', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='phones',
            field=models.TextField(help_text='Use different lines for phones.', default='', verbose_name='Phones', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='company',
            name='slug',
            field=models.SlugField(default='c', verbose_name='Slug'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='company',
            name='web_site',
            field=models.URLField(default='', verbose_name='Web site', blank=True),
            preserve_default=True,
        ),
    ]
