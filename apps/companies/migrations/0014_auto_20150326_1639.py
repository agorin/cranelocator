# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0013_company_scope_of_operation'),
    ]

    operations = [
        migrations.AlterField(
            model_name='company',
            name='name',
            field=models.CharField(max_length=100, verbose_name='Name'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='company',
            name='vat',
            field=models.CharField(max_length=100, verbose_name='VAT Number'),
            preserve_default=True,
        ),
    ]
