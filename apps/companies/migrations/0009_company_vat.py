# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0008_auto_20150325_1058'),
    ]

    operations = [
        migrations.AddField(
            model_name='company',
            name='vat',
            field=models.CharField(default='Not specified', max_length=100, verbose_name='VAT Number'),
            preserve_default=False,
        ),
    ]
