from django.http import HttpResponseForbidden
from django.utils.translation import ugettext as _
from .models import Company, CompanyUserRole


def company_staff_required(view_func):
    def _wrapped_view_func(request, slug, *args, **kwargs):
        user = request.user
        if user.is_authenticated():
            company = Company.objects.get(slug__exact=slug)
            role = CompanyUserRole.objects.get(
                company=company, user=user)

        if user.is_anonymous() or role.role not in ['owner', 'manager']:
            return HttpResponseForbidden(
                _('You don\'t have permissions for manage this company.'))
        else:
            return view_func(request, slug, *args, **kwargs)
    return _wrapped_view_func


def company_owner_required(view_func):
    def _wrapped_view_func(request, slug, *args, **kwargs):
        user = request.user
        if user.is_authenticated():
            company = Company.objects.get(slug__exact=slug)
            role = CompanyUserRole.objects.get(
                company=company, user=user)

        if user.is_anonymous() or role.role != 'owner':
            return HttpResponseForbidden(
                _('You don\'t have permissions for manage this company.'))
        else:
            return view_func(request, slug, *args, **kwargs)
    return _wrapped_view_func
