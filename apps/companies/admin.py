from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from sorl.thumbnail.admin import AdminImageMixin
from .models import (
    BusinessSegment,
    ScopeOfOperation,
    Company,
    CompanyUserRole,
)
from .forms import CompanyAdminForm


class BusinessSegmentAdmin(admin.ModelAdmin):
    list_display = ('label', 'code')
    search_fields = ('label', 'code')
    prepopulated_fields = {"code": ("label",)}


class ScopeOfOperationAdmin(admin.ModelAdmin):
    list_display = ('label', 'code')
    search_fields = ('label', 'code')
    prepopulated_fields = {"code": ("label",)}


class CompanyUserRoleInline(admin.TabularInline):
    model = CompanyUserRole
    suit_classes = 'suit-tab suit-tab-userroles'
    raw_id_fields = ['user']
    extra = 1


class CompanyAdmin(AdminImageMixin, admin.ModelAdmin):
    form = CompanyAdminForm
    prepopulated_fields = {"slug": (
        "name", "invoice_country", "invoice_city")}
    fieldsets = (
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['name', 'slug', 'vat', 'verified'],
        }),
        (_('Categories'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': [
                'business_segments',
                'scope_of_operation'],
        }),
        (_('Description'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['logo', 'short_description', 'description'],
        }),
        (_('Information'), {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': [
                'depots', 'employees'],
        }),
        (_('Visit address'), {
            'classes': ('suit-tab', 'suit-tab-addresses',),
            'fields': [
                'visit_street', 'visit_house_number',
                'visit_additional_info', 'visit_post_code',
                'visit_city', 'visit_country'],
        }),
        (_('Invoice address'), {
            'classes': ('suit-tab', 'suit-tab-addresses',),
            'fields': [
                'invoice_street', 'invoice_house_number',
                'invoice_additional_info', 'invoice_post_code',
                'invoice_city', 'invoice_country'],
        }),
        (_('Company contacts'), {
            'classes': ('suit-tab', 'suit-tab-contacts',),
            'fields': [
                'pnone1', 'pnone2',
                'fax', 'email',
                'web_site'],
        }),
        (_('Contact person 1'), {
            'classes': ('suit-tab', 'suit-tab-contacts',),
            'fields': [
                'cp1_name', 'cp1_position',
                'cp1_phone', 'cp1_email',
                'cp1_skype'],
        }),
        (_('Contact person 2'), {
            'classes': ('suit-tab', 'suit-tab-contacts',),
            'fields': [
                'cp2_name', 'cp2_position',
                'cp2_phone', 'cp2_email',
                'cp2_skype'],
        }),
        (None, {
            'classes': ('suit-tab', 'suit-tab-seo',),
            'fields': ['meta_title', 'meta_description'],
        }),
    )
    suit_form_tabs = (
        ('general', _('General')),
        ('addresses', _('Addresses')),
        ('contacts', _('Contacts')),
        ('seo', _('SEO')),
        ('userroles', _('User roles')),
    )
    list_display = ('name', 'vat', 'verified', 'added', 'last_change')
    search_fields = ['name', 'vat']
    list_filter = ('verified',)
    date_hierarchy = 'last_change'
    inlines = [CompanyUserRoleInline]

admin.site.register(Company, CompanyAdmin)
admin.site.register(BusinessSegment, BusinessSegmentAdmin)
admin.site.register(ScopeOfOperation, ScopeOfOperationAdmin)
