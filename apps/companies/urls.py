from django.conf.urls import patterns, url
from .views import (
    CompanyListView,
    CompanyDetailView,
    company_sidebar,
    create_company,
    manage_company,
    UpdateBusinessSegmentsView,
    UpdateScopeOfOperationView,
    UpdateCompanyLogoView,
    UpdateCompanyDescriptionView,
    UpdateCompanyAddressesView,
    UpdateCompanyContactsView,
    manage_company_staff,
    manage_company_equipment,
    manage_company_equipment_category,
    manage_company_add_equipment,
)

urlpatterns = patterns(
    '',
    url(r'^companies/$', CompanyListView.as_view(), name='company_list'),
    url(r'^companies/add/$', create_company, name='company_add'),
    url(
        r'^companies/(?P<slug>[-\w]+)/$', CompanyDetailView.as_view(),
        name='company_details'),
    url(
        r'^companies/(?P<slug>[-\w]+)/sidebar/$', company_sidebar,
        name='company_sidebar'),

    url(
        r'^companies/(?P<slug>[-\w]+)/manage/$', manage_company,
        name='company_manage'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/edit-business-segments/$',
        UpdateBusinessSegmentsView.as_view(),
        name='company_edit_business_segments'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/edit-scope-of-operation/$',
        UpdateScopeOfOperationView.as_view(),
        name='company_edit_scope_of_operation'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/edit-logo/$',
        UpdateCompanyLogoView.as_view(),
        name='company_edit_logo'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/description/$',
        UpdateCompanyDescriptionView.as_view(),
        name='company_manage_description'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/addresses/$',
        UpdateCompanyAddressesView.as_view(),
        name='company_manage_addresses'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/contacts/$',
        UpdateCompanyContactsView.as_view(),
        name='company_manage_contacts'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/staff/$',
        manage_company_staff,
        name='company_manage_staff'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/equipment/$',
        manage_company_equipment,
        name='company_manage_equipment'),
    url(
        r'^companies/(?P<slug>[-\w]+)/manage/'
        'equipment/(?P<category_slug>[-\w]+)/$',
        manage_company_equipment_category,
        name='company_manage_equipment_category'),

    url(
        r'^companies/(?P<slug>[-\w]+)/manage/'
        'equipment/(?P<category_slug>[-\w]+)/add-equipment/$',
        manage_company_add_equipment,
        name='company_manage_add_equipment'),
)
