from django.db import models
from django.conf import settings
from django.core.exceptions import ValidationError
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone

from sorl.thumbnail import ImageField
from ckeditor.fields import RichTextField
from django_countries.fields import CountryField


# Validators


def validate_company_name(value):
    try:
        Company.objects.get(name__exact=value)
        raise ValidationError(_('Company with such name already exists.'))
    except Company.DoesNotExist:
        return value


def validate_vat(value):
    try:
        Company.objects.get(vat__exact=value)
        raise ValidationError(
            _('Company with such VAT number already exists.'))
    except Company.DoesNotExist:
        return value
    except Company.MultipleObjectsReturned:
        raise ValidationError(_(
            'Multiple companies with the same VAT number are in DB. '
            'Use your database management tool for fix that.'
        ))

# Models


class BusinessSegment(models.Model):
    label = models.CharField(_('Label'), max_length=100)
    code = models.SlugField(_('Code'))

    class Meta:
        verbose_name = _('Business segment')
        verbose_name_plural = _('Business segments')
        ordering = ('label',)

    def __str__(self):
        return self.label


class ScopeOfOperation(models.Model):
    label = models.CharField(_('Label'), max_length=100)
    code = models.SlugField(_('Code'))

    class Meta:
        verbose_name = _('Scope of operation')
        verbose_name_plural = _('Scopes of operation')
        ordering = ('label',)

    def __str__(self):
        return self.label


class Company(models.Model):
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'))
    logo = ImageField(
        _('Logo'), upload_to='companies/logos', blank=True, null=True)
    vat = models.CharField(_('VAT Number'), max_length=100)

    # Description
    short_description = RichTextField(
        _('Short description'), config_name='basic', blank=True, default='')
    description = RichTextField(
        _('Description'), config_name='basic', blank=True, default='')

    # Visit address
    visit_street = models.CharField(
        _('Street'), max_length=255, default='', blank=True)
    visit_house_number = models.CharField(
        _('House number'), max_length=50, default='', blank=True)
    visit_additional_info = models.TextField(
        _('Additional info'), blank=True, default='')
    visit_post_code = models.CharField(
        _('Post code'), max_length=50, default='', blank=True)
    visit_city = models.CharField(
        _('City'), max_length=255, default='', blank=True)
    visit_country = CountryField(_('Country'))

    # Invoice address
    invoice_street = models.CharField(
        _('Street'), max_length=255, default='', blank=True)
    invoice_house_number = models.CharField(
        _('House number'), max_length=50, default='', blank=True)
    invoice_additional_info = models.TextField(
        _('Additional info'), blank=True, default='')
    invoice_post_code = models.CharField(
        _('Post code'), max_length=50, default='', blank=True)
    invoice_city = models.CharField(
        _('City'), max_length=255)
    invoice_country = CountryField(_('Country'))

    # Company contacts
    pnone1 = models.CharField(
        _('First phone'), max_length=50, default='', blank=True,
        help_text=_('Phone number including international code.'))
    pnone2 = models.CharField(
        _('Second phone'), max_length=50, default='', blank=True,
        help_text=_('Phone number including international code.'))
    fax = models.CharField(
        _('Fax'), max_length=50, default='', blank=True,
        help_text=_('Fax number including international code.'))
    email = models.EmailField(
        _('Email'), blank=True, default='')
    web_site = models.URLField(
        _('Web site'), blank=True, default='')

    # Contact persons
    cp1_name = models.CharField(
        _('Full name'), max_length=100, default='', blank=True)
    cp1_position = models.CharField(
        _('Position'), max_length=100, default='', blank=True)
    cp1_phone = models.CharField(
        _('Phone'), max_length=100, default='', blank=True,
        help_text=_('Phone number including international code.'))
    cp1_email = models.EmailField(
        _('Email'), blank=True, default='')
    cp1_skype = models.CharField(
        _('Skype'), max_length=50, default='', blank=True)

    cp2_name = models.CharField(
        _('Full name'), max_length=100, default='', blank=True)
    cp2_position = models.CharField(
        _('Position'), max_length=100, default='', blank=True)
    cp2_phone = models.CharField(
        _('Phone'), max_length=100, default='', blank=True,
        help_text=_('Phone number including international code.'))
    cp2_email = models.EmailField(
        _('Email'), blank=True, default='')
    cp2_skype = models.CharField(
        _('Skype'), max_length=50, default='', blank=True)

    # Depots and employees
    depots = models.IntegerField(_('Depots'), default=0)
    employees = models.IntegerField(_('Employees'), default=0)

    # SEO
    meta_title = models.CharField(
        _('META title'), blank=True, max_length=250,
        default='{{site_name}} - {{company.name}}')
    meta_description = models.TextField(
        _('META description'), blank=True,
        default='{{company.name}}')

    business_segments = models.ManyToManyField(
        BusinessSegment, verbose_name=_('Business segments'),
        null=True, blank=True)
    scope_of_operation = models.ManyToManyField(
        ScopeOfOperation, verbose_name=_('Scope of region operation'),
        null=True, blank=True)

    added = models.DateTimeField(_('Added'), auto_now_add=True)
    last_change = models.DateTimeField(_('Last change'), auto_now=True)

    verified = models.BooleanField(_('Verified'), default=False, blank=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def __str__(self):
        return self.name

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'company_details',
            kwargs={'slug': self.slug})

    def get_roles(self):
        return self.roles.all()

    def get_visit_address_string(self):
        _t = [
            self.visit_post_code,
            self.visit_city,
            self.get_visit_country_display(),
            self.visit_street,
            self.visit_house_number,
            self.visit_additional_info,
        ]
        return ', '.join([i for i in _t if i])

    def get_invoice_address_string(self):
        _t = [
            self.invoice_post_code,
            self.invoice_city,
            self.get_invoice_country_display(),
            self.invoice_street,
            self.invoice_house_number,
            self.invoice_additional_info,
        ]
        return ', '.join([i for i in _t if i])

    def get_categories(self):
        _cs = [bs.label for bs in self.business_segments.all()]
        if _cs:
            return ' | '.join(_cs)
        else:
            return ''

    def get_scope_of_operation(self):
        _ss = [so.label for so in self.scope_of_operation.all()]
        if _ss:
            return ' | '.join(_ss)
        else:
            return ''

    def _get_meta_data(self):
        data = {
            'company': self,
            'site_name': settings.PROJECT_NAME,
        }
        return data

    def _render_meta(self, text):
        from django.template import Context, Template
        context = Context(self._get_meta_data())
        template = Template(text)
        return template.render(context)

    def get_meta_title(self):
        return self._render_meta(self.meta_title)

    def get_meta_description(self):
        return self._render_meta(self.meta_description)


class CompanyUserRole(models.Model):
    COMPANY_ROLE_CHOICES = (
        ('owner', _('Owner')),
        ('manager', _('Manager')),
    )

    company = models.ForeignKey(
        Company, verbose_name=_('Company'),
        related_name='roles')
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, verbose_name=_('User'),
        related_name='company_roles')
    role = models.CharField(
        _('Role'), max_length=20, choices=COMPANY_ROLE_CHOICES)

    class Meta:
        verbose_name = _('Company user role')
        verbose_name_plural = _('Company user roles')

    def __str__(self):
        return '%s - %s' % (self.user, self.company)
