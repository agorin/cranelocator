from django.forms import ModelForm
from django.forms.widgets import TextInput
from django.utils.translation import ugettext_lazy as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit
from crispy_forms.bootstrap import PrependedText
from easy_select2.widgets import Select2, Select2Multiple
from .models import (
    Company,
    validate_company_name,
    validate_vat,
)


class CreateCompanyForm(ModelForm):
    class Meta:
        model = Company
        exclude = ('logo', 'employees', 'depots', 'slug', 'visit_country')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'name',
            'vat',
            'business_segments',
            'scope_of_operation',
            'invoice_city',
            'invoice_country',
            ButtonHolder(
                Submit('submit', _('Create'), css_class='btn btn-primary')
            )
        )
        # TODO: rewrite this
        self.fields['name'].validators = [validate_company_name]
        self.fields['vat'].validators = [validate_vat]


class ChangeBusinessSegmentsForm(ModelForm):
    class Meta:
        model = Company
        fields = ('business_segments',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'business_segments',
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class ChangeScopeOfOperationForm(ModelForm):
    class Meta:
        model = Company
        fields = ('scope_of_operation',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'scope_of_operation',
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class ChangeCompanyLogoForm(ModelForm):
    class Meta:
        model = Company
        fields = ('logo',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'logo',
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class ChangeCompanyDescriptionForm(ModelForm):
    class Meta:
        model = Company
        fields = (
            'short_description', 'description',
            'employees', 'depots')

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            'short_description',
            'description',
            'employees',
            'depots',
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class ChangeCompanyAddressesForm(ModelForm):
    class Meta:
        model = Company
        fields = (
            'visit_street', 'visit_house_number',
            'visit_additional_info', 'visit_post_code',
            'visit_city', 'visit_country',
            'invoice_street', 'invoice_house_number',
            'invoice_additional_info', 'invoice_post_code',
            'invoice_city', 'invoice_country',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                _('Visit address'),
                'visit_street',
                'visit_house_number',
                'visit_additional_info',
                'visit_post_code',
                'visit_city',
                'visit_country',
            ),
            Fieldset(
                _('Invoice address'),
                'invoice_street',
                'invoice_house_number',
                'invoice_additional_info',
                'invoice_post_code',
                'invoice_city',
                'invoice_country',
            ),
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class ChangeCompanyContactsForm(ModelForm):
    class Meta:
        model = Company
        fields = (
            'pnone1', 'pnone2',
            'fax', 'email', 'web_site',
            'cp1_name', 'cp1_position',
            'cp1_phone', 'cp1_email',
            'cp1_skype',
            'cp2_name', 'cp2_position',
            'cp2_phone', 'cp2_email',
            'cp2_skype',
        )

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.layout = Layout(
            Fieldset(
                _('Company contacts'),
                'pnone1',
                'pnone2',
                'fax',
                'email',
                'web_site',
            ),
            Fieldset(
                _('Contact person 1'),
                'cp1_name',
                'cp1_position',
                'cp1_phone',
                'cp1_email',
                'cp1_skype',
            ),
            Fieldset(
                _('Contact person 2'),
                'cp2_name',
                'cp2_position',
                'cp2_phone',
                'cp2_email',
                'cp2_skype',
            ),
            ButtonHolder(
                Submit('submit', _('Save'), css_class='btn btn-primary')
            )
        )


class CompanyAdminForm(ModelForm):
    class Meta:
        model = Company
        fields = '__all__'
        widgets = {
            'business_segments': Select2Multiple,
            'scope_of_operation': Select2Multiple,
        }
