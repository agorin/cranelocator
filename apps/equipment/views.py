from django.shortcuts import render_to_response, redirect, render
from django.http import Http404
from django.views.generic.detail import DetailView
from django.views.generic import ListView

from .models import (
    Manufacturer,
    # EquipmentItem,
    EquipmentCategory,
    Equipment,
    # EquipmentItem,
)


# class ManufacturerListView(ListView):
#     model = Manufacturer
#     context_object_name = 'manufacturers'
#     paginate_by = 20


# class ManufacturerDetailView(DetailView):
#     model = Manufacturer
#     context_object_name = 'manufacturer'

#     def get_context_data(self, **kwargs):
#         context = super().get_context_data(**kwargs)
#         context['categories'] = EquipmentCategory.objects.all()
#         return context


# def equipment_item_view(request, manufacturer_slug, slug, id):
#     try:
#         _item = EquipmentItem.objects.get(
#             equipment__manufacturer__slug=manufacturer_slug,
#             equipment__slug=slug,
#             pk=id)
#     except EquipmentItem.DoesNotExist:
#         raise Http404

#     return render(
#         request,
#         'equipment/equipment_item_detail.html',
#         {
#             'item': _item,
#         })


def equipment_view(request, category_slug, manufacturer_slug, model, id):
    try:
        _eq = Equipment.objects.get(
            category__slug=category_slug,
            manufacturer__slug=manufacturer_slug,
            code__exact=model,
            id=id)
    except Equipment.DoesNotExist:
        raise Http404

    return render(
        request,
        'equipment/equipment_detail.html',
        {
            'equipment': _eq,
        })


# def category_view(request, path, instance):
#     children = instance.get_children()
#     return render(
#         request,
#         'equipment/equipmentcategory_detail.html',
#         {
#             'category': instance,
#             'children': children,
#         })


def equipment_search_view(request):
    from django.contrib.gis.geos import fromstr
    from django.contrib.gis.measure import D

    equipments = Equipment.objects.all()
    categories = EquipmentCategory.objects.all()

    category_filter = request.GET.get('category')
    distance_filter = request.GET.get('distance')
    longitude = request.GET.get('longitude')
    latitude = request.GET.get('latitude')
    _view_style = request.GET.get('view', 'map')

    selected_category = 'all'
    selected_distance = '0'

    if category_filter and category_filter != 'all':
        selected_category = category_filter
        category = EquipmentCategory.objects.get(slug__exact=category_filter)
        equipments = equipments.filter(
            category__in=category.get_descendants(include_self=True)
        ).distinct()

    if distance_filter and distance_filter != '0':
        selected_distance = distance_filter
        _pnt = fromstr(
            'POINT(%s %s)' % (longitude, latitude),
            srid=4326,
        )
        equipments = equipments.filter(
            coordinates__distance_lte=(_pnt, D(km=float(distance_filter))))

    if _view_style == 'list':
        _pnt = fromstr(
            'POINT(%s %s)' % (longitude, latitude),
            srid=4326,
        )
        equipments = equipments.distance(_pnt).order_by('distance')

    return render(
        request,
        'equipment/%s_view.html' % _view_style,
        {
            'view': _view_style,
            'equipments': equipments,
            'categories': categories,
            'selected_category': selected_category,
            'selected_distance': selected_distance,
        })
