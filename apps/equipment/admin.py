from django.contrib.gis import admin
from django.utils.functional import curry
from django.utils.translation import ugettext_lazy as _
from django_mptt_admin.admin import DjangoMpttAdmin
from adminsortable.admin import SortableAdmin, SortableTabularInline

from .models import (
    EquipmentStatusLabel,
    Manufacturer,
    EquipmentCategory,
    Equipment,
    EquipmentProperty,
    EquipmentCategoryProperty,
    EquipmentPropertyValue,
    EquipmentImage,
    EquipmentFile,
    ManufacturerRequest,
)
from .forms import (
    ManufacturerAdminForm,
    EquipmentCategoryAdminForm,
    EquipmentAdminForm,
    EquipmentPropertyValueAdminForm,
    ManufacturerRequestAdminForm
)


class EquipmentStatusLabelAdmin(SortableAdmin):
    list_display = ('label',)
    search_fields = ('label',)


class ManufacturerAdmin(admin.ModelAdmin):
    form = ManufacturerAdminForm
    list_display = ('name', 'slug')
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ('name',)
    fieldsets = (
        (_('General'), {
            'fields': ['name', 'slug', 'logo'],
        }),
        (_('SEO'), {
            'fields': ['meta_title', 'meta_description'],
        }),
    )


class EquipmentCategoryPropertyInline(SortableTabularInline):
    model = EquipmentCategoryProperty
    extra = 1


class EquipmentCategoryAdmin(DjangoMpttAdmin, SortableAdmin):
    form = EquipmentCategoryAdminForm
    list_display = ('name_preview', 'slug')
    prepopulated_fields = {"slug": ("name",)}
    search_fields = ('name',)
    mptt_level_indent = 40
    fieldsets = (
        (_('General'), {
            'fields': ['name', 'slug', 'parent', 'icon'],
        }),
        (_('SEO'), {
            'fields': ['meta_title', 'meta_description'],
        }),
    )
    inlines = [EquipmentCategoryPropertyInline]

    def name_preview(self, obj):
        return '<span style="margin-left:%spx">%s</span>' % (
            self.mptt_level_indent * obj.level,
            obj.name,
        )
    name_preview.short_description = _('Name')
    name_preview.allow_tags = True
    name_preview.admin_order_field = 'name'


class EquipmentPropertyValueInline(admin.TabularInline):
    model = EquipmentPropertyValue
    form = EquipmentPropertyValueAdminForm
    fields = ('equipment_property', 'value')
    extra = 0


class EquipmentImageInline(SortableTabularInline):
    model = EquipmentImage
    readonly_fields = ['uploaded_from_url']
    extra = 1


class EquipmentFileInline(SortableTabularInline):
    model = EquipmentFile
    readonly_fields = ['uploaded_from_url']
    extra = 1


class EquipmentAdmin(admin.ModelAdmin):
    form = EquipmentAdminForm
    list_display = (
        'name', 'manufacturer', 'code', 'category', 'vin',
        'owner', 'address', 'longitude', 'latitude',
        'last_change')
    search_fields = ('name',)
    list_filter = ('manufacturer', 'category',)
    readonly_fields = ('coordinates', 'code')
    fieldsets = (
        (_('General'), {
            'fields': [
                'name', 'vin',
                'category', 'manufacturer',
                'owner',
                'statuses',
            ],
        }),
        (_('Position'), {
            'classes': ('map-wrapper',),
            'fields': [
                'address', 'longitude', 'latitude',
            ],
        }),
        (_('Description'), {
            'fields': [
                'description',
            ],
        }),
    )
    inlines = [
        EquipmentImageInline,
        EquipmentFileInline,
        EquipmentPropertyValueInline,
    ]


class EquipmentPropertyAdmin(admin.ModelAdmin):
    list_display = ('name', 'code', 'type')
    search_fields = ('name', 'code')
    list_filter = ('type',)
    prepopulated_fields = {"code": ("name",)}


class ManufacturerRequestAdmin(admin.ModelAdmin):
    form = ManufacturerRequestAdminForm
    list_display = ('name',)
    search_fields = ('name',)


admin.site.register(EquipmentStatusLabel, EquipmentStatusLabelAdmin)
admin.site.register(Manufacturer, ManufacturerAdmin)
admin.site.register(EquipmentCategory, EquipmentCategoryAdmin)
admin.site.register(Equipment, EquipmentAdmin)
admin.site.register(EquipmentProperty, EquipmentPropertyAdmin)
admin.site.register(ManufacturerRequest, ManufacturerRequestAdmin)
