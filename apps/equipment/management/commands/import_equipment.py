# import os
# import sys
# import csv
# import re
# import requests
# import urllib
# from django.core.files import File
# from django.core.files.temp import NamedTemporaryFile
# from django.core.management.base import BaseCommand, CommandError
# from django.utils.text import slugify
# from ...models import (
#     Manufacturer,
#     Equipment,
#     EquipmentCategory,
#     EquipmentPropertyValue,
#     EquipmentProperty,
#     EquipmentImage,
#     EquipmentFile,
# )


# class Command(BaseCommand):
#     # args = ''
#     help = 'Import equipment from csv from stdin'

#     def _transfer_stdin_to_tempfile(self):
#         content = sys.stdin.read()
#         outfile = open('temp.csv', 'w')
#         outfile.write(content)
#         outfile.close()
#         return outfile.name

#     def handle(self, *args, **options):
#         _r=re.compile(r'<.+>')
#         tempfile_name = self._transfer_stdin_to_tempfile()
#         with open(tempfile_name) as csvfile:
#             reader = csv.DictReader(csvfile, delimiter=';')
#             for i, row in enumerate(reader):
#                 # print (row.keys())
#                 _keys = set(row.keys())
#                 _keys.remove('manufacturer')
#                 _keys.remove('category')
#                 _keys.remove('model')
#                 _keys.remove('description')
#                 _keys.remove('pic-url')
#                 _keys.remove('pic-title')
#                 _keys.remove('file-url')
#                 _keys.remove('file-title')
#                 _keys = list(_keys)

#                 _manufacturer = row.get('manufacturer')
#                 if not _manufacturer:
#                     self.stdout.write(
#                         'Error: Manufacturer not specified in line #%d' % i)
#                     continue
#                 _category = row.get('category')
#                 _model = row.get('model')

#                 try:
#                     _manufacturer_obj = Manufacturer.objects.get(
#                         slug__exact=_manufacturer)
#                 except Manufacturer.DoesNotExist:
#                     self.stdout.write(
#                         'Error: Manufacturer not found - %s. Skipped.' % _manufacturer)
#                     continue

#                 try:
#                     _category_obj = EquipmentCategory.objects.get(
#                         slug__exact=_category)
#                 except EquipmentCategory.DoesNotExist:
#                     self.stdout.write(
#                         'Error: Category not found - %s. Skipped.' % _category)
#                     continue

#                 _description = row.get('description', '')
#                 _d_tags = _r.findall(_description)
#                 if _description and len(_d_tags) == 0:
#                     _description = '<p>' + _description + '</p>'
#                 # print (_description)
#                 try:
#                     _model_obj = Equipment.objects.get(
#                         name__exact=_model,
#                         category=_category_obj,
#                         manufacturer=_manufacturer_obj)
#                     _model_obj.description = _description
#                     _model_obj.save()
#                 except Equipment.DoesNotExist:
#                     self.stdout.write(
#                         'Message: New model found - %s %s. Created.' % (
#                             _manufacturer, _model))
#                     _model_obj = Equipment(
#                         name=_model,
#                         slug=slugify(_model),
#                         category=_category_obj,
#                         manufacturer=_manufacturer_obj,
#                         description=_description
#                     )
#                     _model_obj.save()

#                 # Read the info about images, files and titles
#                 _pic_urls = row.get('pic-url', '').splitlines()
#                 _pic_titles = row.get('pic-title', '').splitlines()
#                 _file_urls = row.get('file-url', '').splitlines()
#                 _file_titles = row.get('file-title', '').splitlines()

#                 # Looking for image downloading
#                 for i, url in enumerate(_pic_urls):
#                     self.stdout.write('> %s' % url)
#                     if not _model_obj.images.filter(
#                         uploaded_from_url__exact=url).exists():
#                         r = requests.get(url)
#                         img_temp = NamedTemporaryFile(delete=True)
#                         img_temp.write(r.content)
#                         img_temp.flush()
#                         try:
#                             _title = _pic_titles[i]
#                         except IndexError:
#                             _title = '%s - Image %s' % (
#                                 str(_model_obj), i)
#                         _img = EquipmentImage(
#                             equipment=_model_obj,
#                             position=i,
#                             image=File(
#                                 name='%s-%s-%s%s' % (
#                                     _model_obj.manufacturer.slug,
#                                     _model_obj.slug,
#                                     i,
#                                     os.path.splitext(url)[1]),
#                                 file=img_temp),
#                             uploaded_from_url=url,
#                             title=_title)
#                         _img.save()
#                     else:
#                         _img_obj = _model_obj.images.get(
#                             uploaded_from_url__exact=url)
#                         try:
#                             _title = _pic_titles[i]
#                         except IndexError:
#                             _title = '%s - Image %s' % (
#                                 str(_model_obj), i)
#                         _img_obj.title = _title
#                         _img_obj.save()

#                 # Looking for file downloading
#                 for i, url in enumerate(_file_urls):
#                     self.stdout.write('>> %s' % url)
#                     if not _model_obj.files.filter(
#                             uploaded_from_url__exact=url).exists():
#                         r = requests.get(url)
#                         file_temp = NamedTemporaryFile(delete=True)
#                         file_temp.write(r.content)
#                         file_temp.flush()
#                         try:
#                             _title = _file_titles[i]
#                         except IndexError:
#                             _title = '%s - File %s' % (
#                                 str(_model_obj), i)
#                         _file = EquipmentFile(
#                             equipment=_model_obj,
#                             position=i,
#                             file=File(
#                                 name='%s-%s-%s%s' % (
#                                     _model_obj.manufacturer.slug,
#                                     _model_obj.slug,
#                                     i,
#                                     os.path.splitext(url)[1]),
#                                 file=file_temp),
#                             uploaded_from_url=url,
#                             title=_title)
#                         _file.save()
#                     else:
#                         _file_obj = _model_obj.files.get(
#                             uploaded_from_url__exact=url)
#                         try:
#                             _title = _file_titles[i]
#                         except IndexError:
#                             _title = '%s - File %s' % (
#                                 str(_model_obj), i)
#                         _file_obj.title = _title
#                         _file_obj.save()

#                 self.stdout.write('%s | %s' % (
#                     _category_obj,
#                     _model_obj)
#                 )
#                 _values = _model_obj.property_values.all()
#                 _prop_codes = [
#                     i.equipment_property.code for i in _values]
#                 for v in _values:
#                     _tv = row.get(v.equipment_property.code, '')
#                     if v.equipment_property.type == v.equipment_property.FLOAT:
#                         _tv = _tv.replace(',', '.')
#                     v.value = _tv
#                     v.save()
#                     self.stdout.write(' | %s = "%s"' % (
#                         v.equipment_property.name, _tv))

#                 _last_pos = _values.count()
#                 for k in _keys:
#                     _last_pos += 1
#                     _tv = row.get(k, '')
#                     if _tv and k not in _prop_codes:
#                         try:
#                             _ep = EquipmentProperty.objects.get(
#                                 code__exact=k)
#                         except EquipmentProperty.DoesNotExist:
#                             self.stdout.write(
#                                 'Error: Property not found - %s. Skipped.' % k)
#                             continue
#                         if _ep.type == _ep.FLOAT:
#                             _tv = _tv.replace(',', '.')
#                         v = EquipmentPropertyValue(
#                             equipment=_model_obj,
#                             equipment_property=_ep,
#                             value=_tv,
#                             position=_last_pos)
#                         v.save()
#                         self.stdout.write(' + %s = "%s"' % (
#                             _ep.name, _tv))
#         os.remove(tempfile_name)
