# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0017_equipmentcategoryproperty'),
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentPropertyValue',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('value_text', models.TextField(null=True, blank=True, verbose_name='Text')),
                ('value_integer', models.IntegerField(null=True, blank=True, verbose_name='Integer')),
                ('value_boolean', models.NullBooleanField(verbose_name='Boolean')),
                ('value_float', models.FloatField(null=True, blank=True, verbose_name='Float')),
                ('position', models.PositiveIntegerField(verbose_name='Position')),
                ('equipment', models.ForeignKey(related_name='property_values', verbose_name='Equipment', to='equipment.Equipment')),
                ('equipment_property', models.ForeignKey(related_name='values', verbose_name='Property', to='equipment.EquipmentProperty')),
            ],
            options={
                'verbose_name': 'Equipment property value',
                'verbose_name_plural': 'Equipment property values',
            },
            bases=(models.Model,),
        ),
    ]
