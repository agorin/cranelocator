# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0028_auto_20150409_1543'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='equipmentstatuslabel',
            options={'verbose_name': 'Status label', 'verbose_name_plural': 'Status labels', 'ordering': ['order']},
        ),
        migrations.RemoveField(
            model_name='equipmentstatuslabel',
            name='position',
        ),
        migrations.AddField(
            model_name='equipmentstatuslabel',
            name='order',
            field=models.PositiveIntegerField(editable=False, db_index=True, default=1),
        ),
    ]
