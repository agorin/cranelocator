# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0009_auto_20150310_1020'),
    ]

    operations = [
        migrations.AddField(
            model_name='manufacturer',
            name='logo',
            field=sorl.thumbnail.fields.ImageField(verbose_name='Logo', blank=True, upload_to='manufacturers/logos', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='manufacturer',
            name='meta_description',
            field=models.TextField(default='{{manufacturer.name}}', verbose_name='META description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='manufacturer',
            name='meta_title',
            field=models.CharField(default='{{site_name}} - {{manufacturer.name}}', verbose_name='META title', blank=True, max_length=250),
            preserve_default=True,
        ),
    ]
