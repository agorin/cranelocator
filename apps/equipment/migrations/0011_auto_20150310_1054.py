# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0010_auto_20150310_1032'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentcategory',
            name='icon',
            field=sorl.thumbnail.fields.ImageField(upload_to='categories/icons', null=True, verbose_name='Icon', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipmentcategory',
            name='meta_description',
            field=models.TextField(default='{{category.name}}', verbose_name='META description', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipmentcategory',
            name='meta_title',
            field=models.CharField(default='{{site_name}} - {{category.name}}', verbose_name='META title', max_length=250, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipmentcategory',
            name='slug',
            field=models.SlugField(default='c', verbose_name='Slug'),
            preserve_default=False,
        ),
    ]
