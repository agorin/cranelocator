# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import adminsortable.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0029_auto_20150409_1817'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='equipmentcategoryproperty',
            options={'verbose_name_plural': 'Equipment category properties', 'ordering': ['order'], 'verbose_name': 'Equipment category property'},
        ),
        migrations.RemoveField(
            model_name='equipmentcategoryproperty',
            name='position',
        ),
        migrations.AddField(
            model_name='equipmentcategoryproperty',
            name='order',
            field=models.PositiveIntegerField(default=1, db_index=True, editable=False),
        ),
        migrations.AlterField(
            model_name='equipmentcategoryproperty',
            name='equipment_category',
            field=adminsortable.fields.SortableForeignKey(to='equipment.EquipmentCategory', verbose_name='Equipment category', related_name='property_links'),
        ),
    ]
