# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0005_auto_20150304_1548'),
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentItem',
            fields=[
                ('id', models.AutoField(serialize=False, auto_created=True, primary_key=True, verbose_name='ID')),
                ('coordinates', django.contrib.gis.db.models.fields.PointField(verbose_name='Coordinates', srid=4326)),
                ('added', models.DateTimeField(verbose_name='Added', auto_now_add=True)),
                ('last_change', models.DateTimeField(auto_now=True, verbose_name='Last change')),
                ('equipment', models.ForeignKey(related_name='items', to='equipment.Equipment', verbose_name='Equipment')),
                ('status', models.ForeignKey(to='equipment.EquipmentStatusLabel', verbose_name='Status')),
            ],
            options={
                'verbose_name_plural': 'Items',
                'verbose_name': 'Item',
            },
            bases=(models.Model,),
        ),
    ]
