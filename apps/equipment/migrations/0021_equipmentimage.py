# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import apps.equipment.models


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0020_auto_20150312_1115'),
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentImage',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, verbose_name='ID', auto_created=True)),
                ('image', sorl.thumbnail.fields.ImageField(blank=True, verbose_name='Image', null=True, upload_to=apps.equipment.models.equipment_image_file_name)),
                ('title', models.CharField(max_length=300, verbose_name='Title')),
                ('position', models.PositiveIntegerField(verbose_name='Position')),
                ('equipment', models.ForeignKey(verbose_name='Equipment', related_name='images', to='equipment.Equipment')),
            ],
            options={
                'verbose_name': 'Equipment image',
                'verbose_name_plural': 'Equipment images',
                'ordering': ('position',),
            },
            bases=(models.Model,),
        ),
    ]
