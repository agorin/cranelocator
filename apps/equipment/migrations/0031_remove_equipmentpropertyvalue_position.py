# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0030_auto_20150409_1823'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipmentpropertyvalue',
            name='position',
        ),
    ]
