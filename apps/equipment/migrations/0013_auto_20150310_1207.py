# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0012_auto_20150310_1105'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipment',
            name='manufacturer',
            field=models.ForeignKey(related_name='equipment', verbose_name='Manufacturer', to='equipment.Manufacturer'),
            preserve_default=True,
        ),
    ]
