# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0007_auto_20150305_1132'),
    ]

    operations = [
        migrations.AddField(
            model_name='manufacturer',
            name='slug',
            field=models.SlugField(default='c', verbose_name='Slug'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='equipmentitem',
            name='address',
            field=models.CharField(max_length=200, blank=True, default='', verbose_name='Address'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='equipmentitem',
            name='latitude',
            field=models.FloatField(help_text='WGS84 Decimal Degree.', verbose_name='Latitude'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='equipmentitem',
            name='longitude',
            field=models.FloatField(help_text='WGS84 Decimal Degree.', verbose_name='Longitude'),
            preserve_default=True,
        ),
    ]
