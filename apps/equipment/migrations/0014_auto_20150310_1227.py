# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0013_auto_20150310_1207'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipment',
            name='meta_description',
            field=models.TextField(blank=True, verbose_name='META description', default='{{equipment.get_full_name}}'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='meta_title',
            field=models.CharField(blank=True, verbose_name='META title', max_length=250, default='{{site_name}} - {{equipment.get_full_name}}'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='slug',
            field=models.SlugField(default='c', verbose_name='Slug'),
            preserve_default=False,
        ),
    ]
