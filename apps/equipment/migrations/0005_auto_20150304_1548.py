# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0004_equipment'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipment',
            name='coordinates',
        ),
        migrations.RemoveField(
            model_name='equipment',
            name='status',
        ),
    ]
