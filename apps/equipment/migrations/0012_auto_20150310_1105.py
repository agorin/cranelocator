# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0011_auto_20150310_1054'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='equipmentcategory',
            unique_together=set([('slug', 'parent')]),
        ),
    ]
