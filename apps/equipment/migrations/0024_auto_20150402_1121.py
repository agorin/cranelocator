# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.contrib.gis.db.models.fields
from django.contrib.gis.geos import GEOSGeometry


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0017_auto_20150401_1935'),
        ('equipment', '0023_equipmentfile'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipment',
            name='address',
            field=models.CharField(verbose_name='Address', blank=True, default='', max_length=200),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='coordinates',
            field=django.contrib.gis.db.models.fields.PointField(verbose_name='Coordinates', default=GEOSGeometry('POINT(0 0)'), srid=4326),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='latitude',
            field=models.FloatField(verbose_name='Latitude', help_text='WGS84 Decimal Degree.', default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='longitude',
            field=models.FloatField(verbose_name='Longitude', help_text='WGS84 Decimal Degree.', default=0),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='owner',
            field=models.ForeignKey(verbose_name='Owner', related_name='equipment', to='companies.Company', default=1),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='equipment',
            name='statuses',
            field=models.ManyToManyField(verbose_name='Statuses', to='equipment.EquipmentStatusLabel'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipment',
            name='verified',
            field=models.BooleanField(verbose_name='Verified', default=False),
            preserve_default=True,
        ),
    ]
