# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0032_auto_20150409_1831'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipment',
            name='address',
            field=models.CharField(blank=True, default='', max_length=200, verbose_name='Address', help_text='Closest destination by Google Maps.'),
        ),
        migrations.AlterField(
            model_name='equipment',
            name='name',
            field=models.CharField(max_length=300, verbose_name='Name', help_text='Only model name, without manufacturer.'),
        ),
    ]
