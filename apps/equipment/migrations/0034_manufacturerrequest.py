# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0033_auto_20150410_1937'),
    ]

    operations = [
        migrations.CreateModel(
            name='ManufacturerRequest',
            fields=[
                ('id', models.AutoField(serialize=False, verbose_name='ID', auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, verbose_name='Name', unique=True)),
                ('requested_for', models.ManyToManyField(related_name='requested_manufacturer', to='equipment.Equipment', verbose_name='Requested for')),
            ],
            options={
                'verbose_name_plural': 'Requested manufacturers',
                'verbose_name': 'Requested manufacturer',
            },
        ),
    ]
