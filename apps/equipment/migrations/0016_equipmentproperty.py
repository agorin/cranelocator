# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0015_auto_20150310_1229'),
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentProperty',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(verbose_name='Name', max_length=150)),
                ('code', models.SlugField(validators=[django.core.validators.RegexValidator(regex='^[a-zA-Z\\-_][0-9a-zA-Z\\-_]*$', message="Code can only contain the letters a-z, A-Z, digits, minus and underscores, and can't start with a digit")], verbose_name='Code', max_length=128)),
                ('type', models.CharField(choices=[('text', 'Text'), ('integer', 'Integer'), ('boolean', 'True / False'), ('float', 'Float')], verbose_name='Type', default='text', max_length=20)),
            ],
            options={
                'verbose_name_plural': 'Equipment properties',
                'verbose_name': 'Equipment property',
                'ordering': ['code'],
            },
            bases=(models.Model,),
        ),
    ]
