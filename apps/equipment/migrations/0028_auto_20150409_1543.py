# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0027_auto_20150409_1538'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipment',
            name='meta_description',
        ),
        migrations.RemoveField(
            model_name='equipment',
            name='meta_title',
        ),
    ]
