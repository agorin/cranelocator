# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import mptt.fields
import ckeditor.fields
import django.contrib.gis.db.models.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0003_equipmentcategory'),
    ]

    operations = [
        migrations.CreateModel(
            name='Equipment',
            fields=[
                ('id', models.AutoField(auto_created=True, serialize=False, verbose_name='ID', primary_key=True)),
                ('name', models.CharField(max_length=300, verbose_name='Name')),
                ('description', ckeditor.fields.RichTextField(verbose_name='Description')),
                ('coordinates', django.contrib.gis.db.models.fields.PointField(verbose_name='Coordinates', srid=4326)),
                ('added', models.DateTimeField(auto_now_add=True, verbose_name='Added')),
                ('last_change', models.DateTimeField(auto_now=True, verbose_name='Last change')),
                ('category', mptt.fields.TreeForeignKey(null=True, verbose_name='Category', related_name='equipments', to='equipment.EquipmentCategory', blank=True)),
                ('manufacturer', models.ForeignKey(to='equipment.Manufacturer', verbose_name='Manufacturer')),
                ('status', models.ForeignKey(to='equipment.EquipmentStatusLabel', verbose_name='Status')),
            ],
            options={
                'verbose_name': 'Equipment',
                'verbose_name_plural': 'Equipments',
            },
            bases=(models.Model,),
        ),
    ]
