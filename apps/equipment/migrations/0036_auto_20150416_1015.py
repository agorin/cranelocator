# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0035_equipment_code'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentproperty',
            name='choices',
            field=models.TextField(blank=True, verbose_name='Choices', help_text='Usable for Choice type. Type choices in different lines.', default=''),
        ),
        migrations.AlterField(
            model_name='equipmentproperty',
            name='type',
            field=models.CharField(default='text', verbose_name='Type', max_length=20, choices=[('text', 'Text'), ('integer', 'Integer'), ('boolean', 'True / False'), ('float', 'Float'), ('choice', 'Choice')]),
        ),
    ]
