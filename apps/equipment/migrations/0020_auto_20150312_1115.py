# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import ckeditor.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0019_equipmentpropertyvalue_value'),
    ]

    operations = [
        migrations.AlterField(
            model_name='equipment',
            name='description',
            field=ckeditor.fields.RichTextField(verbose_name='Description', default='', blank=True),
            preserve_default=True,
        ),
    ]
