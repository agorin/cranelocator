# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0025_equipmentcategoryproperty_required'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='equipmentitem',
            name='equipment',
        ),
        migrations.RemoveField(
            model_name='equipmentitem',
            name='owner',
        ),
        migrations.RemoveField(
            model_name='equipmentitem',
            name='status',
        ),
        migrations.DeleteModel(
            name='EquipmentItem',
        ),
    ]
