# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import sorl.thumbnail.fields
import apps.equipment.models


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0022_equipmentimage_uploaded_from_url'),
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentFile',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, verbose_name='ID', serialize=False)),
                ('file', sorl.thumbnail.fields.ImageField(upload_to=apps.equipment.models.equipment_file_name, blank=True, verbose_name='Image', null=True)),
                ('title', models.CharField(max_length=300, verbose_name='Title')),
                ('uploaded_from_url', models.CharField(default='', max_length=400, blank=True, verbose_name='Uploaded from URL')),
                ('position', models.PositiveIntegerField(verbose_name='Position')),
                ('equipment', models.ForeignKey(verbose_name='Equipment', related_name='files', to='equipment.Equipment')),
            ],
            options={
                'verbose_name_plural': 'Equipment files',
                'verbose_name': 'Equipment file',
                'ordering': ('position',),
            },
            bases=(models.Model,),
        ),
    ]
