# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0034_manufacturerrequest'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipment',
            name='code',
            field=models.CharField(verbose_name='Code', max_length=300, blank=True, default=''),
        ),
    ]
