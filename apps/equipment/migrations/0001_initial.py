# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentStatusLabel',
            fields=[
                ('id', models.AutoField(auto_created=True, verbose_name='ID', primary_key=True, serialize=False)),
                ('label', models.CharField(verbose_name='Label', max_length=100)),
                ('position', models.PositiveIntegerField(verbose_name='Position')),
            ],
            options={
                'ordering': ('position',),
                'verbose_name': 'Status label',
                'verbose_name_plural': 'Status labels',
            },
            bases=(models.Model,),
        ),
    ]
