# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0014_auto_20150310_1227'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='equipment',
            unique_together=set([('slug', 'manufacturer')]),
        ),
    ]
