# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0018_equipmentpropertyvalue'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentpropertyvalue',
            name='value',
            field=models.TextField(verbose_name='Value', null=True, blank=True),
            preserve_default=True,
        ),
    ]
