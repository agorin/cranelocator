# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0024_auto_20150402_1121'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentcategoryproperty',
            name='required',
            field=models.BooleanField(default=True, verbose_name='Required'),
        ),
    ]
