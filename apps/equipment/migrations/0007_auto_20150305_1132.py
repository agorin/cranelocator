# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('companies', '0004_company_short_description'),
        ('equipment', '0006_equipmentitem'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentitem',
            name='address',
            field=models.CharField(help_text='Press "Tab" to refresh the map', max_length=200, verbose_name='Address', default='', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='equipmentitem',
            name='latitude',
            field=models.FloatField(verbose_name='Latitude', default=0, help_text='WGS84 Decimal Degree. Press "Tab" to refresh the map'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='equipmentitem',
            name='longitude',
            field=models.FloatField(verbose_name='Longitude', default=0, help_text='WGS84 Decimal Degree. Press "Tab" to refresh the map'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='equipmentitem',
            name='owner',
            field=models.ForeignKey(verbose_name='Owner', related_name='equipment_items', default=1, to='companies.Company'),
            preserve_default=False,
        ),
    ]
