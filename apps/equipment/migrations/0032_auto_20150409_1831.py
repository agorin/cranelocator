# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import adminsortable.fields


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0031_remove_equipmentpropertyvalue_position'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='equipmentfile',
            options={'verbose_name': 'Equipment file', 'verbose_name_plural': 'Equipment files', 'ordering': ['order']},
        ),
        migrations.AlterModelOptions(
            name='equipmentimage',
            options={'verbose_name': 'Equipment image', 'verbose_name_plural': 'Equipment images', 'ordering': ['order']},
        ),
        migrations.RemoveField(
            model_name='equipmentfile',
            name='position',
        ),
        migrations.RemoveField(
            model_name='equipmentimage',
            name='position',
        ),
        migrations.AddField(
            model_name='equipmentfile',
            name='order',
            field=models.PositiveIntegerField(editable=False, default=1, db_index=True),
        ),
        migrations.AddField(
            model_name='equipmentimage',
            name='order',
            field=models.PositiveIntegerField(editable=False, default=1, db_index=True),
        ),
        migrations.AlterField(
            model_name='equipmentfile',
            name='equipment',
            field=adminsortable.fields.SortableForeignKey(verbose_name='Equipment', to='equipment.Equipment', related_name='files'),
        ),
        migrations.AlterField(
            model_name='equipmentimage',
            name='equipment',
            field=adminsortable.fields.SortableForeignKey(verbose_name='Equipment', to='equipment.Equipment', related_name='images'),
        ),
    ]
