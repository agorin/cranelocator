# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0026_auto_20150409_1536'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipment',
            name='vin',
            field=models.CharField(blank=True, max_length=255, default='', verbose_name='VIN'),
        ),
        migrations.AlterUniqueTogether(
            name='equipment',
            unique_together=set([]),
        ),
        migrations.RemoveField(
            model_name='equipment',
            name='slug',
        ),
    ]
