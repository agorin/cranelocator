# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0016_equipmentproperty'),
    ]

    operations = [
        migrations.CreateModel(
            name='EquipmentCategoryProperty',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, auto_created=True, verbose_name='ID')),
                ('position', models.PositiveIntegerField(verbose_name='Position')),
                ('equipment_category', models.ForeignKey(verbose_name='Equipment category', related_name='property_links', to='equipment.EquipmentCategory')),
                ('equipment_property', models.ForeignKey(verbose_name='Equipment property', related_name='category_links', to='equipment.EquipmentProperty')),
            ],
            options={
                'verbose_name': 'Equipment category property',
                'verbose_name_plural': 'Equipment category properties',
            },
            bases=(models.Model,),
        ),
    ]
