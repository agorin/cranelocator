# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0021_equipmentimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentimage',
            name='uploaded_from_url',
            field=models.CharField(blank=True, verbose_name='Uploaded from URL', max_length=400, default=''),
            preserve_default=True,
        ),
    ]
