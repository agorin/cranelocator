# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('equipment', '0036_auto_20150416_1015'),
    ]

    operations = [
        migrations.AddField(
            model_name='equipmentproperty',
            name='unit',
            field=models.CharField(max_length=100, help_text='Usable for number fields.', default='no', verbose_name='Measurement unit', choices=[('no', 'No units (as is)'), ('m-mm-in', 'Length (m|mm|in)')]),
        ),
    ]
