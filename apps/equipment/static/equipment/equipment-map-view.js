var map_eq;
var geocoder;

function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    var point = new google.maps.LatLng(
        position.coords.latitude, position.coords.longitude);
    map_eq.panTo(point);

    $('input[name="latitude"]').val(position.coords.latitude);
    $('input[name="longitude"]').val(position.coords.longitude);
}

function load_map() {
    var point = new google.maps.LatLng(32, 33);

    var options = {
        zoom: 8,
        center: point,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map_eq = new google.maps.Map(document.getElementById("map_canvas"), options);

    getLocation();
}
