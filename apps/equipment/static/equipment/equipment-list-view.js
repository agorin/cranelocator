function getLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition);
    } else {
        alert("Geolocation is not supported by this browser.");
    }
}

function showPosition(position) {
    $('input[name="latitude"]').val(position.coords.latitude);
    $('input[name="longitude"]').val(position.coords.longitude);
}

function load_list() {
    getLocation();
}
