var map_eq;
var geocoder;


function save_position(point, address) {
    $("#id_address").val(address);
    $("#id_latitude").val(point.lat().toFixed(6));
    $("#id_longitude").val(point.lng().toFixed(6));
    map_eq.panTo(point);
}

function get_lat() {
    return $('#id_latitude').val();
}

function get_lng() {
    return $('#id_longitude').val();
}

function update_map_when_coords_changed() {
    var point = new google.maps.LatLng(get_lat(), get_lng());
    map_eq.panTo(point);

    geocoder.geocode({'latLng': point}, function(results, status) {
        if (status == google.maps.GeocoderStatus.OK && results[0]) {
            $('#id_address').val(results[0].formatted_address);
        }
    });
}

function load_map() {
    var point = new google.maps.LatLng(get_lat(), get_lng());

    var options = {
        zoom: 13,
        center: point,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };

    map_eq = new google.maps.Map(document.getElementById("map_canvas"), options);

    geocoder = new google.maps.Geocoder();

    var marker = new google.maps.Marker({
        map: map_eq,
        position: point,
        draggable: true
    });

    google.maps.event.addListener(marker, 'dragend', function(mouseEvent) {
        geocoder.geocode({'latLng': mouseEvent.latLng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK && results[0]) {
                $('#id_address').val(results[0].formatted_address);
                save_position(mouseEvent.latLng, results[0].formatted_address);
            }
            else {
                save_position(mouseEvent.latLng);
            }
        });
    });

    google.maps.event.addListener(map_eq, 'click', function(mouseEvent){
        marker.setPosition(mouseEvent.latLng);
        geocoder.geocode({'latLng': mouseEvent.latLng}, function(results, status) {
            if (status == google.maps.GeocoderStatus.OK && results[0]) {
                $('#id_address').val(results[0].formatted_address);
                save_position(mouseEvent.latLng, results[0].formatted_address);
            }
            else {
                save_position(mouseEvent.latLng);
            }
        });
    });

    $('#id_address').geocomplete().bind("geocode:result", function(event, result) {
        marker.setPosition(result.geometry.location);
        save_position(result.geometry.location, result.geometry.value);
        $('#id_address').val(result.formatted_address);
    });

    $('#id_longitude').change(update_map_when_coords_changed);
    $('#id_latitude').change(update_map_when_coords_changed);
}


$(document).ready(function() {
    load_map();
});
