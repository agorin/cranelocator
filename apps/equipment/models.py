from django.contrib.gis.db import models
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.utils.text import slugify
from django.db.models.signals import post_save
from django.core.validators import RegexValidator
from django.contrib.gis.geos import GEOSGeometry
from mptt.models import MPTTModel, TreeForeignKey
from sorl.thumbnail import ImageField
from ckeditor.fields import RichTextField
from adminsortable.models import Sortable
from adminsortable.fields import SortableForeignKey
from .settings import (
    PROPERTY_MEASUREMENT_UNITS,
    PROPERTY_MEASUREMENT_UNITS_CHOICES,
)


class EquipmentStatusLabel(Sortable):
    label = models.CharField(_('Label'), max_length=100)

    class Meta(Sortable.Meta):
        verbose_name = _('Status label')
        verbose_name_plural = _('Status labels')

    def __str__(self):
        return self.label


class Manufacturer(models.Model):
    name = models.CharField(
        _('Name'), max_length=100, unique=True)
    slug = models.SlugField(_('Slug'))
    logo = ImageField(
        _('Logo'), upload_to='manufacturers/logos', blank=True, null=True)

    meta_title = models.CharField(
        _('META title'), blank=True, max_length=250,
        default='{{site_name}} - {{manufacturer.name}}')
    meta_description = models.TextField(
        _('META description'), blank=True,
        default='{{manufacturer.name}}')

    class Meta:
        ordering = ('name',)
        verbose_name = _('Manufacturer')
        verbose_name_plural = _('Manufacturers')

    def __str__(self):
        return self.name

    # def _get_meta_data(self):
    #     data = {
    #         'manufacturer': self,
    #         'site_name': settings.PROJECT_NAME,
    #     }
    #     return data

    # def _render_meta(self, text):
    #     from django.template import Context, Template
    #     context = Context(self._get_meta_data())
    #     template = Template(text)
    #     return template.render(context)

    # def get_meta_title(self):
    #     return self._render_meta(self.meta_title)

    # def get_meta_description(self):
    #     return self._render_meta(self.meta_description)

    def get_equipment(self):
        return self.equipment.all().order_by('category')


class EquipmentCategory(MPTTModel):
    name = models.CharField(_('Name'), max_length=100)
    slug = models.SlugField(_('Slug'))
    parent = TreeForeignKey(
        'self', null=True, blank=True,
        verbose_name=_('Parent'),
        related_name='children')

    icon = ImageField(
        _('Icon'), upload_to='categories/icons', blank=True, null=True)

    meta_title = models.CharField(
        _('META title'), blank=True, max_length=250,
        default='{{site_name}} - {{category.name}}')
    meta_description = models.TextField(
        _('META description'), blank=True,
        default='{{category.name}}')

    class Meta:
        unique_together = ('slug', 'parent')
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __str__(self):
        return self.name

    # def get_absolute_url(self):
    #     from django.core.urlresolvers import reverse
    #     return reverse(
    #         'category_details',
    #         kwargs={'path': self.get_path()})

    def _get_meta_data(self):
        data = {
            'category': self,
            'site_name': settings.PROJECT_NAME,
        }
        return data

    def _render_meta(self, text):
        from django.template import Context, Template
        context = Context(self._get_meta_data())
        template = Template(text)
        return template.render(context)

    def get_meta_title(self):
        return self._render_meta(self.meta_title)

    def get_meta_description(self):
        return self._render_meta(self.meta_description)

    def get_dash_prefix(self, value='---'):
        """Gets the string as `value` ("---" by default) times to level.
        Use for adding prefixes with dashes for category list for example.
        """
        return value * self.level


class Equipment(models.Model):
    name = models.CharField(
        _('Name'), max_length=300,
        help_text=_('Only model name, without manufacturer.'))
    code = models.CharField(
        _('Code'), max_length=300, blank=True, default='')
    category = TreeForeignKey(
        EquipmentCategory, null=True, blank=True,
        verbose_name=_('Category'),
        related_name='equipments')
    vin = models.CharField(
        _('VIN'), max_length=255, blank=True, default='')
    manufacturer = models.ForeignKey(
        Manufacturer,
        verbose_name=_('Manufacturer'),
        related_name='equipment')

    verified = models.BooleanField(_('Verified'), default=False, blank=True)
    owner = models.ForeignKey(
        'companies.Company', verbose_name=_('Owner'),
        related_name='equipment')
    statuses = models.ManyToManyField(
        EquipmentStatusLabel, verbose_name=_('Statuses'))

    address = models.CharField(
        _('Address'), max_length=200, blank=True, default='',
        help_text=_('Closest destination by Google Maps.'))
    longitude = models.FloatField(
        _('Longitude'),
        help_text=_('WGS84 Decimal Degree.'), default=0)
    latitude = models.FloatField(
        _('Latitude'),
        help_text=_('WGS84 Decimal Degree.'), default=0)

    coordinates = models.PointField(
        _('Coordinates'), default=GEOSGeometry('POINT(0 0)'))

    description = RichTextField(
        _('Description'), config_name='basic',
        blank=True, default='')

    added = models.DateTimeField(_('Added'), auto_now_add=True)
    last_change = models.DateTimeField(_('Last change'), auto_now=True)

    objects = models.GeoManager()

    class Meta:
        verbose_name = _('Equipment')
        verbose_name_plural = _('Equipments')

    def __str__(self):
        return '%s - %s' % (
            self.get_full_name(),
            str(self.owner),
        )

    def save(self, *args, **kwargs):
        self.coordinates = 'POINT({:f} {:f})'.format(
            self.longitude,
            self.latitude,
        )
        self.code = slugify(self.name)
        super().save(*args, **kwargs)

    def get_absolute_url(self):
        from django.core.urlresolvers import reverse
        return reverse(
            'equipment_details',
            kwargs={
                'category_slug': self.category.slug,
                'manufacturer_slug': self.manufacturer.slug,
                'model': self.code,
                'id': self.id,
            })

    def get_full_name(self):
        return ' '.join([self.manufacturer.name, self.name])

    # def _get_meta_data(self):
    #     data = {
    #         'equipment': self,
    #         'site_name': settings.PROJECT_NAME,
    #     }
    #     return data

    # def _render_meta(self, text):
    #     from django.template import Context, Template
    #     context = Context(self._get_meta_data())
    #     template = Template(text)
    #     return template.render(context)

    # def get_meta_title(self):
    #     return self._render_meta(self.meta_title)

    # def get_meta_description(self):
    #     return self._render_meta(self.meta_description)

    def get_images(self):
        return self.images.all()

    def get_files(self):
        return self.files.all()

    def has_files(self):
        return self.files.all().count() > 0


def equipment_image_file_name(instance, filename):
    """Generate the file name for uploaded equipment images
    """
    return '/'.join([
        'equipment',
        instance.equipment.manufacturer.slug,
        instance.equipment.slug,
        filename])


def equipment_file_name(instance, filename):
    """Generate the file name for uploaded equipment files
    """
    return '/'.join([
        'equipment',
        instance.equipment.manufacturer.slug,
        instance.equipment.slug,
        'files',
        filename])


class EquipmentImage(Sortable):
    equipment = SortableForeignKey(
        Equipment, verbose_name=_('Equipment'),
        related_name='images')
    image = ImageField(
        _('Image'),
        upload_to=equipment_image_file_name,
        blank=True, null=True)
    title = models.CharField(_('Title'), max_length=300)
    uploaded_from_url = models.CharField(
        _('Uploaded from URL'), blank=True, default='',
        max_length=400)

    class Meta(Sortable.Meta):
        verbose_name = _('Equipment image')
        verbose_name_plural = _('Equipment images')

    def __str__(self):
        return '%s - %s' % (
            str(self.equipment),
            self.title,
        )


class EquipmentFile(Sortable):
    equipment = SortableForeignKey(
        Equipment, verbose_name=_('Equipment'),
        related_name='files')
    file = ImageField(
        _('Image'),
        upload_to=equipment_file_name,
        blank=True, null=True)
    title = models.CharField(_('Title'), max_length=300)
    uploaded_from_url = models.CharField(
        _('Uploaded from URL'), blank=True, default='',
        max_length=400)

    class Meta(Sortable.Meta):
        verbose_name = _('Equipment file')
        verbose_name_plural = _('Equipment files')

    def __str__(self):
        return '%s - %s' % (
            str(self.equipment),
            self.title,
        )

    def get_file_extension(self):
        import os
        return os.path.splitext(self.file.path)[1]

    def get_icon_name(self):
        return 'img/mimetype%s.png' % self.get_file_extension()


class EquipmentProperty(models.Model):
    name = models.CharField(_('Name'), max_length=150)
    code = models.SlugField(
        _('Code'), max_length=128,
        validators=[RegexValidator(
            regex=r'^[a-zA-Z\-_][0-9a-zA-Z\-_]*$',
            message=_("Code can only contain the letters a-z, A-Z, digits, "
                      "minus and underscores, and can't start with a digit"))])

    # Property types
    TEXT = "text"
    INTEGER = "integer"
    BOOLEAN = "boolean"
    FLOAT = "float"
    CHOICE = "choice"
    TYPE_CHOICES = (
        (TEXT, _("Text")),
        (INTEGER, _("Integer")),
        (BOOLEAN, _("True / False")),
        (FLOAT, _("Float")),
        (CHOICE, _("Choice")),
    )
    type = models.CharField(
        choices=TYPE_CHOICES, default=TYPE_CHOICES[0][0],
        max_length=20, verbose_name=_("Type"))
    choices = models.TextField(
        _('Choices'), blank=True, default='',
        help_text=_(
            'Usable for Choice type. Type choices in different lines.'))
    unit = models.CharField(
        _('Measurement unit'), default='no',
        max_length=100,
        choices=PROPERTY_MEASUREMENT_UNITS_CHOICES,
        help_text=_('Usable for number fields.'))

    class Meta:
        ordering = ['code']
        verbose_name = _('Equipment property')
        verbose_name_plural = _('Equipment properties')

    def __str__(self):
        return self.name

    def get_unit_info(self):
        _unit = self.unit
        for t in PROPERTY_MEASUREMENT_UNITS:
            if t[0] == _unit:
                return t
        return PROPERTY_MEASUREMENT_UNITS[0]


class EquipmentCategoryProperty(Sortable):
    equipment_category = SortableForeignKey(
        EquipmentCategory, verbose_name=('Equipment category'),
        related_name='property_links')
    equipment_property = models.ForeignKey(
        EquipmentProperty, verbose_name=('Equipment property'),
        related_name='category_links')

    required = models.BooleanField(
        _('Required'), default=True, blank=True)

    class Meta(Sortable.Meta):
        verbose_name = _('Equipment category property')
        verbose_name_plural = _('Equipment category properties')

    def __str__(self):
        return '%s - %s' % (
            str(self.equipment_category),
            str(self.equipment_property),
        )


class EquipmentPropertyValue(models.Model):
    equipment = models.ForeignKey(
        Equipment, verbose_name=_('Equipment'),
        related_name='property_values')
    equipment_property = models.ForeignKey(
        EquipmentProperty, verbose_name=('Property'),
        related_name='values')

    value = models.TextField(_('Value'), blank=True, null=True)
    value_text = models.TextField(_('Text'), blank=True, null=True)
    value_integer = models.IntegerField(_('Integer'), blank=True, null=True)
    value_boolean = models.NullBooleanField(_('Boolean'), blank=True)
    value_float = models.FloatField(_('Float'), blank=True, null=True)

    class Meta:
        verbose_name = _('Equipment property value')
        verbose_name_plural = _('Equipment property values')

    def __str__(self):
        return '%s = %s' % (
            str(self.equipment_property.name),
            str(self.value),
        )

    def save(self, *args, **kwargs):
        self.value_text = self.value
        self.value_boolean = bool(self.value)
        try:
            _f = float(self.value)
            self.value_float = _f
            self.value_integer = int(_f)
        except ValueError:
            self.value_float = self.value_integer = 0

        super().save(*args, **kwargs)

    def get_property_type(self):
        return self.equipment_property.type

    def get_value(self):
        if self.get_property_type() == self.equipment_property.TEXT:
            return self.value_text
        elif self.get_property_type() == self.equipment_property.INTEGER:
            return self.value_integer
        elif self.get_property_type() == self.equipment_property.BOOLEAN:
            return self.value_boolean
        elif self.get_property_type() == self.equipment_property.FLOAT:
            return self.value_float

    def is_number(self):
        return self.get_property_type() in [
            self.equipment_property.INTEGER, self.equipment_property.FLOAT]

    def get_metric_value(self, formatted=True):
        _ui = self.equipment_property.get_unit_info()
        if _ui[0] != 'no':
            _value = self.get_value() * _ui[3]
            if formatted:
                return _ui[5] % _value
            else:
                _value
        else:
            return self.get_value()

    def get_imperial_value(self, formatted=True):
        _ui = self.equipment_property.get_unit_info()
        if _ui[0] != 'no':
            _value = self.get_value() * _ui[4]
            if formatted:
                return _ui[6] % _value
            else:
                _value
        else:
            return self.get_value()


def update_equipment_properties(sender, instance, **kwargs):
    _values = instance.property_values.all()
    if not _values:
        _property_list = instance.category.property_links.all()
        for _i in _property_list:
            _e = EquipmentPropertyValue(
                equipment=instance,
                equipment_property=_i.equipment_property,
                value='')
            _e.save()

post_save.connect(update_equipment_properties, sender=Equipment)


class ManufacturerRequest(models.Model):
    name = models.CharField(
        _('Name'), max_length=100, unique=True)
    requested_for = models.ManyToManyField(
        Equipment,
        verbose_name=_('Requested for'),
        related_name='requested_manufacturer'
    )

    class Meta:
        verbose_name = _('Requested manufacturer')
        verbose_name_plural = _('Requested manufacturers')
