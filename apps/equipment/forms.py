from django.utils.translation import ugettext_lazy as _
from django.forms import (
    CharField,
    ModelForm,
    formsets,
)
from django.forms.widgets import TextInput, CheckboxInput, Select
from django.forms.models import BaseInlineFormSet, inlineformset_factory
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Fieldset, ButtonHolder, Submit, HTML
from crispy_forms.bootstrap import Accordion, AccordionGroup, AppendedText
from easy_select2.widgets import Select2, Select2Multiple
from .models import (
    Manufacturer,
    EquipmentCategory,
    Equipment,
    EquipmentProperty,
    EquipmentPropertyValue,
    ManufacturerRequest,
)


class ManufacturerAdminForm(ModelForm):
    class Meta:
        model = Manufacturer
        fields = '__all__'


class EquipmentCategoryAdminForm(ModelForm):
    class Meta:
        model = EquipmentCategory
        fields = '__all__'


class EquipmentAdminForm(ModelForm):
    class Meta:
        model = Equipment
        fields = '__all__'
        widgets = {
            'manufacturer': Select2,
            'owner': Select2,
            'statuses': Select2Multiple,
        }


class ManufacturerRequestAdminForm(ModelForm):
    class Meta:
        model = ManufacturerRequest
        fields = '__all__'
        widgets = {
            'requested_for': Select2Multiple,
        }


class EquipmentPropertyValueAdminForm(ModelForm):
    class Meta:
        model = EquipmentPropertyValue
        fields = ['equipment_property', 'value']
        # widgets = {
        #     'value': TextInput(),
        # }

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        _instance = kwargs.get('instance')
        if _instance:
            if _instance.equipment_property.type == _instance.equipment_property.TEXT:
                self.fields['value'].widget = TextInput()
            elif _instance.equipment_property.type == _instance.equipment_property.INTEGER:
                self.fields['value'].widget = TextInput() # NumberInput()
            elif _instance.equipment_property.type == _instance.equipment_property.FLOAT:
                self.fields['value'].widget = TextInput()
            elif _instance.equipment_property.type == _instance.equipment_property.BOOLEAN:
                self.fields['value'].widget = CheckboxInput()
            elif _instance.equipment_property.type == _instance.equipment_property.CHOICE:
                _values = _instance.equipment_property.choices.splitlines()
                _choices = [(t, t) for t in _values]
                self.fields['value'].widget = Select(choices=_choices)




class AddEquipmentToCompanyForm(ModelForm):
    other_manufacturer = CharField(
        label=_('Manufacturer'), required=False)

    class Meta:
        model = Equipment
        fields = [
            'name', 'manufacturer', 'vin', 'description',
            'statuses',
            'address', 'longitude', 'latitude',
        ]

    class Media:
        js = [
            'equipment/equipment-form-map.js',
            'http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.18/jquery-ui.min.js',
            'http://maps.google.com/maps/api/js?sensor=false&libraries=places',
            'vendor/geocomplete/jquery.geocomplete.min.js',
        ]

    def __init__(self, *args, **kwargs):
        company = kwargs.pop('company')
        equipment_category = kwargs.pop('equipment_category')

        super().__init__(*args, **kwargs)
        self.helper = FormHelper()

        _pls = equipment_category.property_links.all()
        _property_field_names = []
        _property_fields = []
        for p in _pls:
            _property_field_names.append(p.equipment_property.code)
            _property_fields.append('property-%s' % p.equipment_property.code)
            _field_name = 'property-%s' % p.equipment_property.code
            self.fields[_field_name] = CharField(
                label=p.equipment_property.name,
                required=p.required)
            if p.equipment_property.type == p.equipment_property.BOOLEAN:
                self.fields[_field_name].widget = CheckboxInput()
            elif p.equipment_property.type == p.equipment_property.CHOICE:
                _values = p.equipment_property.choices.splitlines()
                _choices = [(t, t) for t in _values]
                self.fields[_field_name].widget = Select(choices=_choices)

        self.fields['vin'].required = True

        _layout = [
            Fieldset(
                _('General'),
                'manufacturer',
                HTML(_(
                    '<p id="__manufacturer-info">'
                    'If you don\'t see a right manufacturer in '
                    'list click <a id="__action--add-manufacturer"'
                    ' class="__action-link">'
                    'this link</a> and type it. '
                    'We will review it and if everything is fine we will '
                    'add the support to it.</p>')),
                'other_manufacturer',
                HTML(_(
                    '<p id="__other-manufacturer-info">If you want to '
                    'return the manufacturer select box click '
                    '<a id="__action--return-manufacturer"'
                    ' class="__action-link">'
                    'this link</a>.'
                    '</p>')),
                'name',
                'vin',
            ),
            Fieldset(
                _('Position and status'),
                'address',
                'longitude',
                'latitude',
                'statuses',
            ),
            Fieldset(
                _('Extra'),
                'description',
            )
        ]
        _layout.append(
            Fieldset(
                _('Properties'),
                *_property_fields
            )
        )
        _layout.append(ButtonHolder(
            Submit('submit', _('Save'), css_class='btn btn-primary')
        ))
        self.helper.layout = Layout(*_layout)
