from django.utils.translation import ugettext_lazy as _
from django.conf import settings


"""
The table with standard mesurement units including metric and imperial
with string formatting. Using when render an equipment page and filtering.

    id, Text description, internal unit abbr, metric coeff, imperial coeff,
        metric format string, imperial format string
"""
_PROPERTY_MEASUREMENT_UNITS = (
    ('no', _('No units (as is)'), '', 0, 0, '%s', '%s'),
    (
        'm-mm-in', _('Length (m|mm|in)'),
        'm', 1000, 40.81, _('%.0f mm'), _('%.2f in')),
)
PROPERTY_MEASUREMENT_UNITS = getattr(
    settings,
    'EQUIPMENT_PROPERTY_MEASUREMENT_UNITS',
    _PROPERTY_MEASUREMENT_UNITS)
PROPERTY_MEASUREMENT_UNITS_CHOICES = \
    [(i[0], i[1]) for i in PROPERTY_MEASUREMENT_UNITS]
