from django.conf.urls import patterns, url
import mptt_urls
from .views import (
    # ManufacturerListView,
    # ManufacturerDetailView,
    equipment_search_view,
    # category_view,
    equipment_view,
    # equipment_item_view,
)

urlpatterns = patterns(
    '',
    # url(
    #     r'^manufacturers/$',
    #     ManufacturerListView.as_view(),
    #     name='manufacturer_list'),
    # url(
    #     r'^manufacturers/(?P<slug>[-\w]+)/$',
    #     ManufacturerDetailView.as_view(),
    #     name='manufacturer_details'),
    # url(
    #     r'^equipment/(?P<manufacturer_slug>[-\w]+)/(?P<slug>[-\w]+)/(?P<id>\d+)/$',
    #     equipment_item_view,
    #     name='equipment_item_details'),
    url(
        r'^equipment/(?P<category_slug>[-\w]+)/(?P<manufacturer_slug>[-\w\d]+)/(?P<model>[-\w\d]+)/(?P<id>\d+)/$',
        equipment_view,
        name='equipment_details'),
    # url(
    #     r'^categories/(?P<path>[-\w/]*)/$',
    #     mptt_urls.view(
    #         model='apps.equipment.models.EquipmentCategory',
    #         view=category_view,
    #         slug_field='slug'),
    #     name='category_details'),
    # url(
    #     r'^categories/(?P<slug>[-\w]+)/$',
    #     EquipmentCategoryDetailView.as_view(),
    #     name='category_details'),
    url(r'^search/$', equipment_search_view, name='search'),
)
